#include "include/MemExpWindow.hpp"


MemExp::TimerThread::TimerThread () : timerLog (L"timer_log.txt") {
	win = NULL; 
	time = 0; 
	slidePollInterval = 30; 
} 
MemExp::TimerThread::TimerThread (BasicWindow::Window * window) : timerLog (L"timer_log.txt") {
	win = window; 
	time = 0; 
	slidePollInterval = 30; 
} 


MemExp::TimerThread::~TimerThread () {

} 

std::shared_ptr<MemExp::SlideSheet> MemExp::TimerThread::getCurrentSlide () const { 
	return nowPlaying; 
} 

void MemExp::TimerThread::addSlide (std::shared_ptr<SlideSheet> slide) { 
	slideQueue.push_back (slide); 
} 

void MemExp::TimerThread::requestRedraw () {
	if (!win || !win->hWnd) return; 
	timerLog << "Time: " << time << "; Requesting Window Redraw ... " << std::endl << std::endl; 
	InvalidateRect (win->hWnd, NULL, false); 
}

double MemExp::TimerThread::getTime () const { 
	return ((double) (std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now ().time_since_epoch () - timerStarted)).count () 
			) / 1000.0; 
} 

int MemExp::TimerThread::threadEntry () { 
	timerStarted = std::chrono::system_clock::now ().time_since_epoch (); 
	auto msStart = std::chrono::duration_cast<std::chrono::milliseconds> (timerStarted).count (); 
	auto now = timerStarted; 
	auto elapsed = msStart; 
	unsigned int prevExecCount= 0; 
	unsigned int waitTime = 0; 
	unsigned int tmp; 
	bool needUpdate = true; 
	bool stopLoop = false; 
	double dNowPlayingStartedAt; // Start 'time' of nowPlaying slide. 
	Command cmd; 
	Command old; 
	while (time) Sleep (10); // Wait for initialization to be complete. 
	while (!needClose) { 
		unsigned int i; 
		now = std::chrono::system_clock::now ().time_since_epoch (); 
		elapsed = ((std::chrono::duration_cast<std::chrono::milliseconds> (now - timerStarted)).count ()); 
		time = elapsed * 0.001; 
		// Add any commands whose time it already came to the now-executing buffer: 
		if (!slideQueue.empty ()) { 
			// Pop a slide sheet into 'nowPlaying': 
			nowPlaying = *slideQueue.begin (); // Take value. 
			slideQueue.erase (slideQueue.begin ()); // Remove from list. 
			// Render the slide and wait: 
			requestRedraw (); 
			if (nowPlaying->duration > 0) 
				Sleep ((unsigned int)(nowPlaying->duration * 1000)); 
		} else { 
			if (nowPlaying && nowPlaying->duration > 0) { 
				// Only delete for non-indefinite durations ... 
				nowPlaying = nullptr; 
				requestRedraw (); 
			} 
			Sleep (slidePollInterval); 
		} 
#ifdef _DEBUG 
		retired.push_back (nowPlaying); 
#endif 
	} 
	return 0; 
} 

void MemExp::TimerThread::close () {
	needClose = true; 
	win = NULL; 
} 
