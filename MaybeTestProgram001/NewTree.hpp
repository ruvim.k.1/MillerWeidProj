#ifndef NEWTREE 
#define NEWTREE 
#include <vector> 
template <typename T> 
class NewTree { 
	public: 
	// Properties: 
		int sortDirection; 
		unsigned int nsub; 
		unsigned int heapSize; 
		std::vector<double> weights; 
		std::vector<T> objects; 
		T dummy; 
	// Methods: 
		NewTree (){ 
			init (2); 
		} 
		NewTree (unsigned int nbranches){ 
			if (nbranches) init (nbranches); 
			else init (1); // Need a minimum of 1 branches. 
		} 
		NewTree (NewTree & from){ 
			nsub= from.nsub; 
			sortDirection= from.sortDirection; 
			heapSize= from.heapSize; 
			objects= from.objects; 
			weights= from.weights; 
			dummy= from.dummy; 
		} 
		void init (unsigned int n){ 
			nsub= n; 
			sortDirection= -1; 
			heapSize= 0; 
			objects.resize (1, dummy); 
			weights.resize (1, 0); 
		} 
		virtual unsigned int parent (unsigned int index) const { 
			return (index + nsub - 2) / nsub; 
		} 
		virtual unsigned int child (unsigned int parent, unsigned int nchild) const { 
			return (nsub * parent - nsub + 2) + nchild; 
		} 
		virtual double weight (unsigned int index){ 
			if (index >= weights.size ()) return 0; 
			return weights[index]; 
		} 
		virtual bool needSwap (unsigned int lower, unsigned int higher){ 
			return ((sortDirection < 0)? (weight (lower) < weight (higher)): (weight (lower) > weight (higher))); 
		} 
		virtual void swap (unsigned int a, unsigned int b){ 
			T tmp= objects[a]; 
			double tmpWeight= weights[a]; 
			objects[a]= objects[b]; 
			weights[a]= weights[b]; 
			objects[b]= tmp; 
			weights[b]= tmpWeight; 
		} 
		virtual bool exists (unsigned int index){ 
			if (index && index <= heapSize) return true; 
			else return false; 
		} 
		virtual void heapifyUp (unsigned int start){ 
			unsigned int now= start; 
			unsigned int up= parent (now); 
			while (up && needSwap (now, up)){ 
				if (!exists (now)) { 
					now = up; 
					continue; 
				} 
				swap (now, up); 
				now= up; 
				up= parent (now); 
			} 
		} 
		virtual void heapify (unsigned int start){ 
			unsigned int now= start; 
			unsigned int index; 
			unsigned int down, i; 
			while (now){ 
				if (!exists (now)) break; 
				down= child (now, 0); 
				if (!exists (down)) break; 
				for (i= 1; i < nsub; i++){ 
					index= child (now, i); 
					if (!exists (index)){ 
						i= nsub; 
						continue; 
					} 
					if (needSwap (index, down)) down= index; 
				} 
				if (!needSwap (down, now)) break; 
				swap (now, down); 
				now= down; 
			} 
		} 
		void push (double weight, T object){ 
			unsigned int sz; 
			objects.push_back (object); 
			weights.push_back (weight); 
			heapSize = objects.size () - 1; 
			if (heapSize > 1) { 
				sz = heapSize; 
				heapifyUp (sz); 
			} 
		} 
		T pop (){ 
			return pop (nullptr); 
		} 
		T pop (std::vector<T> & also_save_to){ 
			return pop (&also_save_to); 
		} 
		T pop (std::vector<T> * also_save_to){ 
			T obj= peek_top (); 
			if (!heapSize) return dummy; 
			else if (heapSize == 1){ 
				if (also_save_to) also_save_to->push_back (objects[1]); 
				objects.resize (1); 
				weights.resize (1); 
				heapSize= 0; 
				return obj; 
			} 
			objects[1]= objects[heapSize]; 
			weights[1]= weights[heapSize]; 
			heapSize--; 
			heapify (1); 
			if (also_save_to) also_save_to->push_back (obj); 
			objects.resize (1 + heapSize); 
			weights.resize (1 + heapSize); 
			return obj; 
		} 
		T peek_top (){ 
			if (heapSize) return objects[1]; 
			else return dummy; 
		} 
		unsigned int size (){ 
			return heapSize; 
		} 
		std::vector<T> getUnorderedArray (){ 
			std::vector<T> all; 
			if (!heapSize) return all; 
			all.reserve (heapSize); 
			all.insert (all.begin (), objects.begin () + 1, objects.begin () + heapSize + 1); 
			return all; 
		} 
}; 
#endif 