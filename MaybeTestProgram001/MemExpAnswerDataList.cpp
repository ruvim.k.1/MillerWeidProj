#include "include/MemExpWindow.hpp" 

std::shared_ptr<LIST::Head> MemExp::AnswerDataList::readHead (LIST::IfStream * file) { 
	std::shared_ptr<LIST::Head> pointer = std::make_shared<MemExp::List::AnswerDataHead> (); 
	MemExp::List::AnswerDataHead * head = (MemExp::List::AnswerDataHead *)pointer.get (); 
	std::vector<std::wstring> list; 
	std::wstring line = file->getLine (); 
	std::wstring text; 
	unsigned int i = 0; 
	do { 
		text = L""; 
		for (; i < line.size () && line.at (i) != ','; i++) text.push_back (line.at (i)); 
		list.push_back (text); 
		if (i < line.size () && line.at (i) == ',') i++; 
	} while (i < line.size ()); 
	head->head = list; 
	return pointer; 
} 
std::shared_ptr<LIST::Item> MemExp::AnswerDataList::readItem (LIST::IfStream * file) { 
	std::shared_ptr<LIST::Item> pointer = std::make_shared<MemExp::List::AnswerDataItem> (); 
	MemExp::List::AnswerDataItem * item = (MemExp::List::AnswerDataItem *)pointer.get (); 
	AnswerData * data = &item->data; 
	std::wstring line = file->getLine (); 
	if (!data->setCSV (line)) { 
		// If setCSV () returns false, then an error has occurred; 
		// relay this message to the callee by returning a NULL pointer: 
		pointer = nullptr; 
	} 
	return pointer; 
} 

void MemExp::AnswerDataList::writeHead (LIST::OfStream * file, LIST::Head * pointer) { 
	MemExp::List::AnswerDataHead * head = (MemExp::List::AnswerDataHead *)pointer; 
	std::vector<std::wstring> * row = &head->head; 
	unsigned int i; 
	for (i = 0; i < row->size (); i++) { 
		if (i) file->stream << ","; 
		file->stream << row->at (i); 
	} 
	file->stream << "\r\n"; 
} 
void MemExp::AnswerDataList::writeItem (LIST::OfStream * file, LIST::Item * pointer) { 
	MemExp::List::AnswerDataItem * item = (MemExp::List::AnswerDataItem *)pointer; 
	AnswerData * data = &item->data; 
	std::wstring row = data->getCSV (); 
	file->stream << row; 
	file->stream << "\r\n"; 
} 

