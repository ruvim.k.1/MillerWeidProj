#include "include/ListTextFile.hpp" 

namespace LIST { 
	void fxnCheckBOM (std::wifstream * stream, FileStream * fs) { 
		std::ifstream::pos_type init_ofs = stream->tellg (); 
		unsigned char a, b; 
		// Seek to the beginning of the file: 
		stream->seekg (0); 
		a = (unsigned char)stream->get (); 
		b = (unsigned char)stream->get (); 
		// Check the BOM: 
		if (a == 0xEF || a == 0xBB || a == 0xBF) { 
			// UTF-8 
			fs->bomSize = 1; 
			fs->bitCount = 0; 
		} else if (a == 0xFE && b == 0xFF) { 
			// UTF-16; big-endian; 
			fs->bomSize = 2; 
			fs->bigEndianByteOrder = true; 
		} else if (a == 0xFF && b == 0xFE) { 
			// UTF-16; little-endian; 
			fs->bomSize = 2; 
			fs->bigEndianByteOrder = false; 
		} else { 
			fs->bomSize = 0; 
			fs->bitCount = 8; 
		} 
		// Seek back to original position: 
		stream->seekg (init_ofs); 
	} 
}; 

LIST::FileStream::FileStream () { 
	bigEndianByteOrder = false; 
	bomSize = 0; 
	bitCount = 16; 
} 

void LIST::FileStream::checkBOM (std::wifstream stream) { 
	
} 
void LIST::FileStream::checkBOM (std::wofstream stream) { 

} 
