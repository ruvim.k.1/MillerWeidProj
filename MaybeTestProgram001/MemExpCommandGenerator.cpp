#include "include/MemExpWindow.hpp" 

#define SET_COLOR(x, r, g, b) x.setColor (r, g, b) 
#define PT(x) ((unsigned int)((double)(x) * (1.0 / 72) * settings->dblParams[L"ppi"])) 

MemExp::CommandGenerator::CommandGenerator () { 
	maskPlusTarget = (double)500 / 1000; // 500 milliseconds. 
	exactPrimeTime = 0; // Stage 1 default is to randomly choose between prime times. 
	avlPrimeTimes.push_back ((double)50 / 1000); // 50 milliseconds. 
	avlPrimeTimes.push_back ((double)1000 / 1000); // 1000 milliseconds. 
	targetTimeA = 0.0; 
	targetTimeB = 1.0; 
	targetTimeNow = maskPlusTarget; 
	targetTimeBase = 0; 
	targetTimeRange = maskPlusTarget; 
	msgCorrect.text = L"\u2713"; 
	msgCorrect.color.g = 255; 
	msgIncorrect.text = L"\u2717"; 
	msgIncorrect.color.r = 255; 
	feedbackTime = 2; 
	answerCount = 0; 
	monitorFreq = 0; 
	currentWelcome = 0; 
	desiredLinearTargetCount = 2; 
	primeNeedMatchTarget = primeNeedMatchFoil = false; 
	welcomeDone = false; 
	settings = nullptr; 
	draft = std::make_shared<AnswerData> (); 
} 

MemExp::Number MemExp::CommandGenerator::getTime (Window * expWindow) { 
	auto now = std::chrono::system_clock::now ().time_since_epoch ();
	auto elapsed = (std::chrono::duration_cast<std::chrono::milliseconds> (now - expWindow->timerThread->startT)).count ();
	Number time = ((double)elapsed * 0.001);
	return time; 
} 
MemExp::Number MemExp::CommandGenerator::timerDelay (Window * expWindow) { 
	return 50; // (double)expWindow->timerThread->minSleepTime / 1000; 
} 

unsigned int MemExp::CommandGenerator::countEntriesInCSV (std::wstring filename) { 
	AnswerDataList list; 
	list.load (filename); 
	return list.size (); 
} 
void MemExp::CommandGenerator::loadFromCSV (std::wstring filename, DataMap & map) { 
	std::shared_ptr<LIST::Item> rawItem = nullptr; 
	std::shared_ptr<AnswerData> answerEntry = nullptr; 
	MemExp::List::AnswerDataItem * dataItem = NULL; 
	AnswerDataList list; 
	unsigned int i; 
	list.load (filename); 
	for (i = 0; i < list.size (); i++) { 
		rawItem = list[i]; // Take the list item. 
		dataItem = (MemExp::List::AnswerDataItem *)rawItem.get (); // Turn it into an AnswerDataItem pointer. 
		answerEntry = std::make_shared<AnswerData> (dataItem->data); // Make a copy of the answer data part. 
		// Add the answer data entry to the data map buffer: 
		map.addSample ({ answerEntry->targetTime }, { answerEntry->stddevUsed }, answerEntry->pCorrect, answerEntry); 
		// Add the answer data entry to the quest algorithm: 
		quest.addData (answerEntry->targetTime, ((answerEntry->pCorrect > 0.1)? true: false)); 
		// Add the words to the already-asked list: 
		markAsAsked (answerEntry->prime.text); 
		markAsAsked (answerEntry->target.text); 
		markAsAsked (answerEntry->right.text); 
		markAsAsked (answerEntry->left.text); 
		// Increment answer count: 
		answerCount++; 
		// Update the target-display time: 
		adjustTargetTime (map); 
		// Do anything else: 
		onaddanswer (answerEntry.get (), map); 
	} 
	adjustTargetTime (map); 
} 
void MemExp::CommandGenerator::onaddanswer (AnswerData * answer, DataMap & map) { 

} 
void MemExp::CommandGenerator::onaddnewanswer (AnswerData * answer, DataMap & map) { 

} 
void MemExp::CommandGenerator::postWelcome (Window * expWindow, Number additional_delay) { 
	Number time = getTime (expWindow) + additional_delay; 
	if (currentWelcome >= welcomeFiles.size ()) { 
		currentWelcome = 0; 
		welcomeDone = true; 
		return; 
	} 
	LIST::IfStream sWelcome (welcomeFiles[currentWelcome].c_str ()); 
	SlideTypes::SingleCenteredWord sheet; 
	std::wstring welcomeText = L""; 
	//welcome.time = time; 
	//welcome.stop = 0; // Never. Unless otherwise interrupted. 
	while (sWelcome.canRead ()) { 
		welcomeText += sWelcome.getLine (); 
	} 
	sheet.command ().textType = Multiline | WordBreak; 
	sheet.command ().fontSize = expWindow->settings.intParams[L"Welcome-Text Font Size"]; 
	sheet.setWord (Color (255, 255, 255, 255)); 
	sheet.setWord (welcomeText); 
	expWindow->timerThread->addSlide (std::make_shared<SlideTypes::SingleCenteredWord> (sheet)); 
	currentWelcome++; 
} 
void MemExp::CommandGenerator::putFeedback (Command userAnswer, DataMap & map, Window * expWindow, Number additional_delay) { 
	Number time = getTime (expWindow) + additional_delay; // Slight delay. To ensure timerThread sees it on time. 
	Number wasCorrect; 
	Number stddev = 0.25; 
	SlideTypes::SingleCenteredWord msg; 
	auto duration = std::chrono::system_clock::now ().time_since_epoch (); 
	auto msClock = std::chrono::duration_cast<std::chrono::milliseconds> (duration).count (); 
	Number nowClockTime = (double)msClock / 1000; 
	if (userAnswer.type == L"init") {
		// This putFeedback () call is just an initialization routine, not an actual answer check. 
		return; 
	}
	dataEntry = std::make_shared<AnswerData> (); // Make new user answer entry. 
	// Determine whether the answer was correct: 
	if (userAnswer.text == theTarget.text) { 
		msg.setWord (msgCorrect.text); 
		msg.setWord (msgCorrect.color); 
		wasCorrect = 1; 
		quest.addData (targetClear - targetDrawn, true); 
	} else { 
		msg.setWord (msgIncorrect.text); 
		msg.setWord (msgIncorrect.color); 
		wasCorrect = 0; 
		quest.addData (targetClear - targetDrawn, false); 
	} 
	if (settings) stddev = settings->dblParams[L"Sample Standard Deviation"]; 
	stddev *= 3 * 1.0 / monitorFreq; 
	// Prepare the data entry for data bank insertion: 
	*dataEntry = *draft; 
	dataEntry->clockTime = nowClockTime; 
	dataEntry->primeTime = primeClear - primeDrawn; 
	dataEntry->targetTime = targetClear - targetDrawn; 
	dataEntry->answerTime = getTime (expWindow) - answerDrawn; 
	dataEntry->stddevUsed = stddev; 
	dataEntry->pCorrect = wasCorrect; 
	dataEntry->prime = thePrime; 
	dataEntry->target = theTarget; 
	dataEntry->left = ansLeft; 
	dataEntry->right = ansRight; 
	// Do anything else that is necessary to the answer entry: 
	onaddnewanswer (dataEntry.get (), map); 
	// Add the answer to the data bank: 
	map.addSample ({ targetClear - targetDrawn }, { stddev }, wasCorrect, dataEntry); 
	// If there is a CSV filename present: 
	if (csvFile.size ()) { 
		// Write the user answer data entry to the CSV file: 
		std::wofstream csv (csvFile.c_str (), std::ios::app | std::ios::out | std::ios::binary); 
		std::wstring row = dataEntry->getCSV (); 
		if (csv.is_open ()) { 
			// File is open for appending; can write now. 
			csv << row; 
			csv << "\r\n"; 
		} else { 
			// Some error happened. 
		} 
	} 
	// Add a correct or incorrect message to the display buffer: 
	msg.duration = feedbackTime; 
	expWindow->timerThread->addSlide (std::make_shared<SlideTypes::SingleCenteredWord> (msg)); 
	answerCount++; 
	adjustTargetTime (map); 
} 

#define PTR(x)  std::make_shared<decltype (x)> (x) 
void MemExp::CommandGenerator::generate (Window * expWindow, Number additional_delay) { 
	Number time = getTime (expWindow) + additional_delay; // Slight delay. To ensure timerThread sees it on time. 
	std::vector<Command> sign; 
	std::vector<Command> prime; 
	std::vector<Command> mask; 
	std::vector<Command> target; 
	std::vector<Command> result; 
	SlideTypes::SingleCenteredWord sSign; 
	SlideTypes::DoubleCenteredWord sPrime; 
	SlideTypes::SingleCenteredWord sMask; 
	SlideTypes::SingleCenteredWord sTarget; 
	unsigned int i, j, c; 
	// Begin: 
	beginGenerateProcedure (); 
	// Resize: 
	sign.resize (1); // One sign. 
	prime.resize (2); // Two prime texts. 
	mask.resize (1); // One mask. 
	target.resize (1); // One target. 
	// Do some basic determining: 
	determineWords (expWindow); // Override determineWords () to change prime-selection properties, etc. 
	determineTimes (time); // Override determineTimes () to change time-selection rules, etc. 
	determineStyles (); // Things like font color, size, etc. 
	// Fill out the prime, mask, and target properties: 
	sSign.duration = signTime; 
	sPrime.duration = primeTime; 
	sMask.duration = maskTime; 
	sTarget.duration = targetTime; 
	// Word Text: 
	sSign.setWord (theSign.text); 
	sPrime.setWord (thePrime.text); 
	sMask.setWord (theMask.text); 
	sTarget.setWord (theTarget.text); 
	//// Useless ?: 
	//for (i = 0; i < sign.size (); i++) { 
	//	sign[i] = theSign; 
	//} 
	//for (i = 0; i < prime.size (); i++) { 
	//	prime[i] = thePrime; 
	//} 
	//for (i = 0; i < mask.size (); i++) { 
	//	mask[i] = theMask; 
	//} 
	//for (i = 0; i < target.size (); i++) { 
	//	target[i] = theTarget; 
	//} 
	//// Align the prime, mask, and target: 
	//if (settings->strParams[L"Show"] == L"TogetherTest") { 
	//	for (i = 0; i < sign.size (); i++) { 
	//		sign[i].time = time; 
	//		sign[i].stop = 0; 
	//		sign[i].vAlign = AlignTop; 
	//		sign[i].hAlign = AlignLeft; 
	//	} 
	//	for (i = 0; i < prime.size (); i++) { 
	//		prime[i].time = time; 
	//		prime[i].stop = 0; 
	//		if (i) {
	//			prime[i].top = 0.5; 
	//			prime[i].bottom = 1; 
	//			prime[i].hAlign = AlignCenter; 
	//			prime[i].vAlign = AlignTop; 
	//		} else {
	//			prime[i].top = 0; 
	//			prime[i].bottom = 0.5; 
	//			prime[i].hAlign = AlignCenter; 
	//			prime[i].vAlign = AlignBottom; 
	//		} 
	//	} 
	//	for (i = 0; i < mask.size (); i++) { 
	//		mask[i].time = time; 
	//		mask[i].stop = 0; 
	//		mask[i].bottom = 1; 
	//		mask[i].vAlign = AlignBottom; 
	//		mask[i].hAlign = AlignCenter; 
	//	} 
	//	for (i = 0; i < target.size (); i++) { 
	//		target[i].time = time; 
	//		target[i].stop = 0; 
	//		target[i].vAlign = AlignTop; 
	//		target[i].hAlign = AlignCenter; 
	//	} 
	//	Command msg; 
	//	msg = msgCorrect; 
	//	msg.time = time; 
	//	msg.stop = 0; 
	//	msg.vAlign = AlignBottom; 
	//	msg.hAlign = AlignLeft; 
	//	expWindow->timerThread->addCommand (msg); 
	//	msg = msgIncorrect; 
	//	msg.time = time; 
	//	msg.stop = 0; 
	//	msg.vAlign = AlignBottom; 
	//	msg.hAlign = AlignRight; 
	//	expWindow->timerThread->addCommand (msg); 
	//} else { 
	//	alignAuto (sign); 
	//	alignAuto (prime); 
	//	alignAuto (mask); 
	//	alignAuto (target); 
	//} 
	//// Add the prime, mask, and target to the command queue: 
	//for (i = 0; i < sign.size (); i++) expWindow->timerThread->addCommand (sign[i]); 
	//for (i= 0; i < prime.size (); i++) expWindow->timerThread->addCommand (prime[i]); 
	//for (i = 0; i < target.size (); i++) expWindow->timerThread->addCommand (target[i]); 
	//for (i = 0; i < mask.size (); i++) expWindow->timerThread->addCommand (mask[i]); 
	//result = expWindow->timerThread->commands; // .getUnorderedArray (); 
	//c = 0; 
	//for (i = 0; i < result.size (); i++) { 
	//	for (j = i + 1; j < result.size (); j++) { 
	//		if (result[i].time == result[j].time) c++; 
	//	} 
	//} 
	//if (!c) { 
	//	return theMask.stop; // Something is wrong. 
	//} 
	//return theMask.stop; 
	expWindow->timerThread->addSlide (PTR (sSign)); 
	expWindow->timerThread->addSlide (PTR (sPrime)); 
	expWindow->timerThread->addSlide (PTR (sTarget)); 
	expWindow->timerThread->addSlide (PTR (sMask)); 
	determineAnswerOptions (expWindow); // Determines what options appears on the left and right sides of the screen. 
} 

void MemExp::CommandGenerator::beginGenerateProcedure () { 

} 

void MemExp::CommandGenerator::determineWords (Window * expWindow) { 
	std::wstring err = sampleWord (nullptr); 
	theTarget.text = sampleWord (expWindow); 
	thePrime.text = sampleWord (expWindow); 
	theFoil.text = theTarget.text; 
	theMask.text = L"@@@@@@"; 
	theSign.text = L"\u271A"; 
	if (settings && settings->strParams[L"Sign Text"].size ()) theSign.text = settings->strParams[L"Sign Text"]; 
	while (theFoil.text == theTarget.text && theFoil.text != err) theFoil.text = sampleWord (expWindow); 
	if (primeNeedMatchTarget) { 
		thePrime.text = theTarget.text; 
	} else if (primeNeedMatchFoil) { 
		thePrime.text = theFoil.text; 
	} 
} 

void MemExp::CommandGenerator::determineTimes (Number time) { 
	std::uniform_real_distribution<> dist (0, 1); 
	Number a, b; 
	// Set the sign time (the sign is the warning that appears to let the user know that the word is coming): 
	signTime = 1; 
	if (!exactPrimeTime) { // If there is no specific prime time to use (default for stage 1): 
		a = (double)avlPrimeTimes.size () * dist (generator); 
		primeTime = avlPrimeTimes[(unsigned int)std::floor (a)]; 
	} else primeTime = exactPrimeTime; // Otherwise, use the specific time. 
	// Dynamically calculate target time, but hold targetTime + maskTime constant: 
	targetTime = targetTimeNow; 
	if (monitorFreq) { 
		a = (std::fmod (targetTime, (1.0 / monitorFreq))) * monitorFreq; 
		b = std::floor (targetTime * monitorFreq) / monitorFreq; 
		if (dist (generator) < a) b += (1.0 / monitorFreq); 
		targetTime = b; 
	} 
	maskTime = maskPlusTarget - targetTime; 
} 
void MemExp::CommandGenerator::determineStyles () { 

	theMask.fontFace = settings->strParams[L"Mask Font Face"]; 
	theSign.fontFace = settings->strParams[L"Sign Font Face"]; 
	thePrime.fontFace = settings->strParams[L"Prime Font Face"]; 
	theTarget.fontFace = settings->strParams[L"Target Font Face"]; 
	answerTemplate.fontFace = settings->strParams[L"Answer Option Font Face"]; 
	theMask.fontSize = PT (settings->intParams[L"Mask Font Size"]); 
	theSign.fontSize = PT (settings->intParams[L"Sign Font Size"]); 
	thePrime.fontSize = PT (settings->intParams[L"Prime Font Size"]); 
	theTarget.fontSize = PT (settings->intParams[L"Target Font Size"]); 
	answerTemplate.fontSize = PT (settings->intParams[L"Answer Option Font Size"]); 
	msgCorrect.fontSize = PT (settings->intParams[L"Correct Checkmark Size"]); 
	msgIncorrect.fontSize = PT (settings->intParams[L"Incorrect \"X\" Size"]); 
} 

void MemExp::CommandGenerator::determineAnswerOptions (Window * expWindow) {
	std::uniform_real_distribution<> dist (0, 1); 
	Command left = answerTemplate; 
	Command right = answerTemplate; 
	// Randomly decide whether target should appear on left or right: 
	if (dist (generator) < 0.5) { 
		left.text = theTarget.text; 
		right.text = theFoil.text; 
	} else { 
		left.text = theFoil.text; 
		right.text = theTarget.text; 
	} 
	ansLeft = left; 
	ansRight = right; 
	SlideTypes::TwoWordsOnTwoSides optionSlide; 
	optionSlide.setLeft (left.text); 
	optionSlide.setRight (right.text); 
	expWindow->timerThread->addSlide (PTR (optionSlide)); 
} 

void MemExp::CommandGenerator::adjustTargetTime (DataMap & map) { 
	double prevDuration = targetTimeNow; 
	targetTimeNow = quest.T_post (1.0 / monitorFreq, maskPlusTarget); 
} 

std::wstring MemExp::CommandGenerator::sampleWord (Window * expWindow) { 
	std::wstring error = L"Error:  Not Found"; 
	std::wstring value; 
	std::uniform_real_distribution<> uniform (0, 1); 
	unsigned long long total_words = 0; 
	unsigned long long rand_index; 
	unsigned long long sum; 
	unsigned int i; 
	bool alreadyHaveAsked = false; 
	if (!expWindow) return error; 
	for (i = 0; i < expWindow->lists.size (); i++) { 
		if (i < expWindow->enabledLists.size () && !expWindow->enabledLists[i]) continue; 
		total_words += expWindow->lists[i]->readItemCount; 
	} 
	if (!total_words) return error; 
	do {
		rand_index = (unsigned long long)(std::floor ((double)total_words * uniform (generator)));
		for (i = 0, sum = 0; i < expWindow->lists.size () && sum + expWindow->lists[i]->readItemCount <= rand_index; i++) {
			if (i < expWindow->enabledLists.size () && !expWindow->enabledLists[i]) continue;
			sum += expWindow->lists[i]->readItemCount;
		} 
		value = expWindow->lists[i]->items[(unsigned int)(rand_index - sum)]->value; 
		// For the trials to be independent, we do checking to make sure we do not sample a single word multiple times: 
		alreadyHaveAsked = false; 
		for (i = 0; i < alreadyAsked.size (); i++) { 
			if (value != alreadyAsked[i]) continue; 
			alreadyHaveAsked = true; 
			i = alreadyAsked.size (); 
		} 
	} while (alreadyHaveAsked); 
	markAsAsked (value); 
	return value.substr (0, value.find (L'\r')); 
} 
void MemExp::CommandGenerator::markAsAsked (std::wstring theWord) { 
	unsigned int i; 
	for (i = 0; i < alreadyAsked.size (); i++) if (alreadyAsked[i] == theWord) return; // Already have. 
	alreadyAsked.push_back (theWord); 
} 

void MemExp::CommandGenerator::prepTime (Window * expWindow, DataMap & map, CommandGenerator * prev) { 
	adjustTargetTime (map); 
} 
void MemExp::CommandGenerator::wrapUp (Window * expWindow, DataMap & map) { 
	targetTimeFinal = quest.T_like (1.0 / monitorFreq, maskPlusTarget); 
} 

bool MemExp::CommandGenerator::done () { 
	// Stage 1 is only for the first 60 trials: 
	if (answerCount >= 10) return true; 
	// Check if we can be done early or not: 
	unsigned int hCount = quest.countHypotheses (1.0 / (2.0 * monitorFreq), 0, 1); 
	if (hCount < 2) { 
		// 1 or less hypotheses remaining; confident enough, eh? 
		// We use 2 * (monitor refresh rate) 
		// because we want slightly more accurate, 
		// but not-as-accurate is just fine! 
		// By all means, feel free to take out the 2.0 * if need 
		// to improve program execution speed a little bit. 
		return true; 
	} 
	return false; 
} 

void MemExp::CommandGenerator::alignLeft (Command & cmd) { 
	cmd.left = 0; 
	cmd.top = 0; 
	cmd.right = 1; 
	cmd.bottom = 1; 
	cmd.hAlign = AlignLeft; 
	cmd.vAlign = AlignMiddle; 
} 
void MemExp::CommandGenerator::alignRight (Command & cmd) {
	cmd.left = 0;
	cmd.top = 0;
	cmd.right = 1;
	cmd.bottom = 1;
	cmd.hAlign = AlignRight;
	cmd.vAlign = AlignMiddle;
}
void MemExp::CommandGenerator::alignMiddle (Command & cmd) { 
	cmd.left = 0; 
	cmd.top = 0; 
	cmd.right = 1; 
	cmd.bottom = 1; 
	cmd.hAlign = AlignCenter; 
	cmd.vAlign = AlignMiddle; 
} 
void MemExp::CommandGenerator::alignTop (Command & cmd) { 
	cmd.left = 0; 
	cmd.right = 1; 
	cmd.top = 0; 
	cmd.bottom = 0.5; 
	cmd.hAlign = AlignCenter; 
	cmd.vAlign = AlignBottom; 
} 
void MemExp::CommandGenerator::alignBot (Command & cmd) { 
	cmd.left = 0; 
	cmd.right = 1; 
	cmd.top = 0.5; 
	cmd.bottom = 1; 
	cmd.hAlign = AlignCenter; 
	cmd.vAlign = AlignTop; 
} 

void MemExp::CommandGenerator::alignAuto (std::vector<Command> & arr) { 
	unsigned int i; 
	for (i = 0; i < arr.size (); i++) { 
		if (arr.size () > 1) { 
			if (i) alignBot (arr[i]); 
			else alignTop (arr[i]); 
		} else alignMiddle (arr[i]); 
	} 
} 
