#include "include/ListTextFile.hpp" 

LIST::DataFile::DataFile () { 

} 
LIST::DataFile::DataFile (std::wstring filename) { 
	load (filename); 
} 

void LIST::DataFile::load (std::wstring file_path) { 
	std::shared_ptr<IfStream> input = openForReading (file_path); 
	load (input.get ()); 
} 
void LIST::DataFile::save (std::wstring file_path) { 
	std::shared_ptr<OfStream> output = openForWriting (file_path); 
	save (output.get ()); 
} 

void LIST::DataFile::load (IfStream * file) { 
	if (!file->stream.is_open ()) return; 
	unsigned int i; 
	std::shared_ptr<Item> item; 
	header = readHead (file); 
	wasItemCount = header->itemCount; // Number of items the header says there is. 
	items.resize ((unsigned int)header->itemCount); // Empty the items vector. 
	file->stream.seekg (file->stream.beg + header->ofsFirstItem); 
	if (!wasItemCount) { 
		i = 0; 
		while (item = readItem (file)) { 
			items.push_back (item); 
			i++; 
		} 
	} else for (i = 0; i < header->itemCount && (item = readItem (file)); i++) items[i]= item; 
	readItemCount = i; // Number of items actually read. 
	if (readItemCount < wasItemCount) { 
		items.resize (i); 
		header->update (items); 
	} 
} 
void LIST::DataFile::save (OfStream * file) { 
	if (!file->stream.is_open ()) return; 
	std::ofstream::pos_type ofsHead = file->stream.tellp (); 
	std::ofstream::pos_type ofsStop; 
	unsigned long long sHead; 
	unsigned int i; 
	header->update (items); 
	sHead = header->write (); // Get header size. 
	file->stream.seekp (sHead + ofsHead); 
	for (i = 0; i < items.size (); i++) writeItem (file, items[i].get ()); // Write the list item. 
	ofsStop = file->stream.tellp (); 
	file->stream.seekp (ofsHead); 
	writeHead (file, header.get ()); // Write the header. 
	file->stream.seekp (ofsStop); 
} 

std::shared_ptr<LIST::IfStream> LIST::DataFile::openForReading (std::wstring path) { 
	std::shared_ptr<IfStream> input = std::make_shared<IfStream> (path.c_str (), std::ios::binary); 
	input->stream.seekg (input->bomSize); 
	return input; 
} 
std::shared_ptr<LIST::OfStream> LIST::DataFile::openForWriting (std::wstring path) { 
	std::shared_ptr<OfStream> output = std::make_shared<OfStream> (path.c_str (), std::ios::binary); 
	return output; 
} 
std::shared_ptr<LIST::Head> LIST::DataFile::readHead (IfStream * file) { 
	std::shared_ptr<Head> head = std::make_shared<Head> (); 
	bool ok= head->read (file); 
	if (!ok) head = nullptr; 
	return head; 
}
std::shared_ptr<LIST::Item> LIST::DataFile::readItem (IfStream * file) { 
	std::shared_ptr<Item> item = std::make_shared<Item> (); 
	bool ok= item->read (file); 
	if (!ok) item = nullptr; 
	return item; 
} 
void LIST::DataFile::writeHead (OfStream * file, Head * head) { 
	head->write (file); 
} 
void LIST::DataFile::writeItem (OfStream * file, Item * item) { 
	item->write (file); 
} 

void LIST::DataFile::updateHead () { 
	header->update (items); 
} 

unsigned int const LIST::DataFile::size () { 
	return items.size (); 
} 

std::shared_ptr<LIST::Item> & LIST::DataFile::at (unsigned int index) { 
	return items[index]; 
} 
std::shared_ptr<LIST::Item> & LIST::DataFile:: operator [] (unsigned int index) { 
	return items[index]; 
} 

