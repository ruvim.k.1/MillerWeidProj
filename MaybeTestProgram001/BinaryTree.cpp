#ifndef BinaryTree_IS_DEFINED 
#define BinaryTree_IS_DEFINED 1 
#include "N_aryTree.cpp" 
template <typename T> 
class BinaryTree : public N_aryTree<T> { 
	public: 
	int parent (int index){ 
		// Returns the integer index of the parent node. 
		return ((int)(index >> 1)); // Automatically rounds down. 
	} 
	int child (int index, int nchild){ 
		// Returns the integer index of the n-th child node (n = 0 for the first child). 
		return (index << 1) + nchild; 
	} 
	public: 
	BinaryTree (){ 
		// 0-th element is empty for priority heap implementation 
		N_aryTree<T>::objects.insert (N_aryTree<T>::objects.begin (), NULL); 
		N_aryTree<T>::weights.insert (N_aryTree<T>::weights.begin (), NULL); 
	} 
}; 
#endif 