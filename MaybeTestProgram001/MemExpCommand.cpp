#include "include/MemExpWindow.hpp" 


MemExp::Command::Command () : color (0, 0, 0, 1) { 
	// Blank-object constructor. 
	fontSize = 10; 
	hAlign = AlignLeft; 
	vAlign = AlignTop; 
	textType = Singleline; 
	left = 0; 
	top = 0; 
	right = 1; 
	bottom = 1; 
	bold = italic = underline = strikethrough = false; 
} 
MemExp::Command::Command (int initType) : color (0, 0, 0, 1) { 
	fontSize = 10; 
	hAlign = AlignLeft; 
	vAlign = AlignTop; 
	textType = Singleline; 
	left = 0; 
	top = 0; 
	right = 1; 
	bottom = 1; 
	bold = italic = underline = strikethrough = false; 
	switch (initType) { 
	case TypeCentered: 
		hAlign = AlignCenter; 
		vAlign = AlignMiddle; 
		break; 
	case TypeDoubleCenteredUpper: 
		hAlign = AlignCenter; 
		vAlign = AlignBottom; 
		top = 0.5; 
		break; 
	case TypeDoubleCenteredLower: 
		hAlign = AlignCenter; 
		vAlign = AlignTop; 
		bottom = 0.5; 
		break; 
	case TypeLeftmost: 
		hAlign = AlignLeft; 
		vAlign = AlignMiddle; 
		break; 
	case TypeRightmost: 
		hAlign = AlignRight; 
		vAlign = AlignMiddle; 
		break; 
	} 
} 
void MemExp::Command::copyFrom (Command & from) { 
	type = from.type; 
	text = from.text; 
	fontFace = from.fontFace; 
	fontSize = from.fontSize; 
	hAlign = from.hAlign; 
	vAlign = from.vAlign; 
	textType = from.textType; 
	color = from.color; 
	left = from.left; 
	top = from.top; 
	right = from.right; 
	bottom = from.bottom; 
	bold = from.bold; 
	italic = from.italic; 
	underline = from.underline; 
	strikethrough = from.strikethrough; 
} 


MemExp::Command::~Command () {

} 

std::wstring MemExp::Command::toString () { 
	std::wstringstream ss; 
	std::wstring result; 
	ss << "Command: (" << fontSize << "px " << fontFace << "; align: " << hAlign << ", " << vAlign << "; "; 
	ss << "type: " << textType << "; box: [" << left << ", " << top << ", " << right
		<< ", " << bottom << "]; "; 
	ss << "): ";
	if (text.size ()) ss << "{" << text.substr (1) << "} "; 
	result = ss.str (); 
	return result; 
} 

void MemExp::Command::win32draw (HDC hdc, Box client) { 
	int fw = bold? FW_BOLD: FW_NORMAL; 
	RECT r; 
	HFONT hFont = CreateFont (fontSize, 0, 0, 0, fw, italic, underline, strikethrough, ANSI_CHARSET, 
							  OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, 
							  fontFace.c_str ()); 
	//HBRUSH hBrush = CreateSolidBrush (RGB (colorR, colorG, colorB)); 
	HGDIOBJ prevFont = SelectObject (hdc, hFont); 
	//HGDIOBJ prevBrush = SelectObject (hdc, hBrush); 
	COLORREF prevColor = GetTextColor (hdc); 
	int prevBkMode = GetBkMode (hdc); 
	SetTextColor (hdc, RGB (color.r, color.g, color.b)); 
	SetBkMode (hdc, TRANSPARENT); 
	r.left = (unsigned int)(client.w * left + client.x); 
	r.top = (unsigned int)(client.h * top + client.y); 
	r.right = (unsigned int)(client.w * right + client.x); 
	r.bottom = (unsigned int)(client.h * bottom + client.y); 
	DrawText (hdc, text.c_str (), text.size (), &r, hAlign | vAlign | textType); 
	SelectObject (hdc, prevFont); 
	SetTextColor (hdc, prevColor); 
	SetBkMode (hdc, prevBkMode); 
	//SelectObject (hdc, prevBrush); 
	DeleteObject (hFont); 
	//DeleteObject (hBrush); 
} 

void MemExp::Command::setColor (unsigned char r, unsigned char g, unsigned char b) { 
	color.r = r; 
	color.g = g; 
	color.b = b; 
	color.a = 255; 
} 
void MemExp::Command::setColor (Color theColor) {
	color = theColor; 
} 

