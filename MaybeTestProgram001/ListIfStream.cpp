#include "include/ListTextFile.hpp" 

LIST::IfStream::IfStream () {

} 
LIST::IfStream::IfStream (const wchar_t * filename, std::ios::openmode mode) { 
	open (filename, mode); 
} 

void LIST::IfStream::open (const wchar_t * filename, std::ios::openmode mode) { 
	stream.open (filename, mode); 
	// Determine file size: 
	auto needPosition = stream.tellg (); 
	stream.seekg (0, std::ios::end); 
	fileSize = stream.tellg (); 
	stream.seekg (needPosition, std::ios::beg); 
	auto after = stream.tellg (); 
	// Check BOM: 
	fxnCheckBOM (&stream, this); 
} 

std::wstring LIST::IfStream::getLine () { 
	// Unlike the C++ std::getline () function, this method does, in fact, include the '\n' character. 
	std::wstring line; 
	wchar_t c; 
	while (stream) { 
		c = getWide (); 
		if (!c) break; 
		line.push_back (c); 
		if (c == 10) break; 
	} 
	return line; 
} 

wchar_t LIST::IfStream::getWide () { 
	char a; 
	char b; 
	wchar_t c; 
	wchar_t d; 
	unsigned long long now; 
	if (!stream) return 0; 
	now = stream.tellg (); 
	if (now >= fileSize) { 
		return 0; 
	} 
	if (!bitCount) { 
		// utf-8; I actually have not learned how to parse utf-8 yet, so ... 
		stream.read (&c, 1); 
	} else if (bitCount == 8) { 
		stream.read (&c, 1); 
	} else { 
		c = stream.get (); 
		if (!stream) { 
			return c; 
		} 
		d = stream.get (); 
		a = (char)c; 
		b = (char)d; 
		if (bigEndianByteOrder) c = (a << 8) + b; 
		else c = a + (b << 8); 
	} 
	if (c == 52428) return 0; // For some reason the last character of the read string was 52428, though the text file did not have a word like that. 
	return c; 
} 

bool LIST::IfStream::canRead () { 
	unsigned long long now; 
	if (!stream) return false; 
	now = stream.tellg (); 
	if (now >= fileSize) { 
		return false; 
	} 
	return true; 
} 

