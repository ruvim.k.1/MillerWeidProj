#pragma once

#include <Quest.hpp> 

class MemExp::QuestImplementation : public Quest { 
	public: 
	// Properties: 
		double gamma; 
		double beta; 
		double eps; 
		// Psi-Shape-Related: 
		double x0; // Auto-Filled Upon Construction. 
		double wantP; // Set only upon construction. 
		// Stopping Criteria: 
		double chiSqAlpha; 
		double chiSqDF; 
	// Methods: 
		QuestImplementation (); 
		QuestImplementation (double arg_wantP); 
		virtual ~QuestImplementation (); 
		double psi (double x) const; 
		double f_T (double x) const; 
		unsigned int countHypotheses (double hypothesis_size_dx, double minX, double maxX, int n = -1) const; 
		// countHypotheses uses the stopping criteria method from [Watson and Pelli, 1983] (the Quest paper) 
		// to count how many T_j hypotheses are still present (in other words, measure the integer multiple 
		// uncertainty of the T_like PDF so far). 
		// Parameters: 
		// hypothesis_size_dx tells how big each step is; this is a discretization step size for searching hypotheses 
		// minX is the x value to start the search from 
		// maxX is the x value to stop the search at 
		// n is the number of data entries to take into account (see Quest::quest, for example); -1 to use all data 
}; 

