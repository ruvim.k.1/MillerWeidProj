#include "include\Window.hpp" 

#include <sstream> 

unsigned int BasicWindow::counter= 0; 

BasicWindow::Window::Window () {
	hInst = GetModuleHandle (NULL); 
	hPrevInst = NULL; 
	CmdLine = GetCommandLine (); 
	CmdShow = SW_SHOWDEFAULT; 
	showString = L"normal"; 
	std::wstringstream clsName; 
	clsName.str (L""); 
	clsName << L"WindowClass"; 
		counter++; 
	clsName << counter; 
	wClassName = clsName.str ();  
	wc.cbSize = sizeof (WNDCLASSEX); 
	wc.style = 0; 
	wc.lpfnWndProc = &Window::staticWndProc; 
	wc.cbClsExtra = 0; 
	wc.cbWndExtra = 0; 
	wc.hInstance = hInst; 
	wc.hIcon = LoadIcon (NULL, IDI_APPLICATION); 
	wc.hCursor = LoadCursor (NULL, IDC_ARROW); 
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); 
	wc.lpszMenuName = NULL; 
	wc.lpszClassName = wClassName.c_str (); 
	wc.hIconSm = wc.hIcon; 
	if (!RegisterClassEx (&wc)) { 
		DWORD error = GetLastError (); 
		std::cout << "Error:  Window class registration failed. "; 
		std::cout << "Code: " << error << std::endl; 
		//MessageBox (0, L"Class could not be registered. ", 0, 0);
	} 
	hWnd = NULL; 
	parent = NULL; 
	closed = false; 
	position.x = CW_USEDEFAULT; 
	position.y = CW_USEDEFAULT; 
} 


BasicWindow::Window::~Window () {

} 

int BasicWindow::Window::messageLoop () { 
	HWND hParent = NULL; 
	if (parent) hParent = parent->hWnd; 
	hWnd = CreateWindowEx (0, wClassName.c_str (), wWinText.c_str (), WS_OVERLAPPEDWINDOW, 
						   position.x, position.y, position.w, position.h, 
						   hParent, 
						   NULL, 
						   hInst, 
						   this); 
	if (!hWnd) { 
		DWORD error = GetLastError (); 
		std::cout << "Error:  Window could not be created. "; 
		std::cout << "Code: " << error << std::endl; 
		//MessageBox (0, L"Window could not be created. ", 0, 0); 
	} 
	show (showString); 
	UpdateWindow (hWnd); 
	while (GetMessage (&msg, NULL, 0, 0)) { 
		TranslateMessage (&msg); 
		DispatchMessage (&msg); 
	} 
	return msg.wParam; 
} 

/*************************************** Override-able handlers: *********************************************/ 

void BasicWindow::Window::onnccreate () { 

} 
void BasicWindow::Window::oncreate () { 

} 
void BasicWindow::Window::onkeydown (wchar_t keyCode) { 

} 
void BasicWindow::Window::onkeyup (wchar_t keyCode) { 

} 
void BasicWindow::Window::onchar (wchar_t keyCode) { 

} 
void BasicWindow::Window::onmousedown (int clientX, int clientY) { 

} 
void BasicWindow::Window::onmousemove (int clientX, int clientY) { 

} 
void BasicWindow::Window::onmouseup (int clientX, int clientY) { 

} 
void BasicWindow::Window::onrightdown (int clientX, int clientY) { 

} 
void BasicWindow::Window::onrightup (int clientX, int clientY) { 

} 
void BasicWindow::Window::onmove (int x, int y) { 

} 
void BasicWindow::Window::onresize (int x, int y) { 
	InvalidateRect (hWnd, NULL, true); // Redraw the whole window, including its background. 
} 
void BasicWindow::Window::oncommand (int type, int id) { 

} 
void BasicWindow::Window::ondestroy () { 

} 
void BasicWindow::Window::drawWin32 (HDC hdc) { 

} 
bool BasicWindow::Window::requestClose () { 
	return true; // OK to close by default. 
} 

void BasicWindow::Window::close () { 
	CloseWindow (hWnd); 
} 

/*************************************** The WndProc stuff: **************************************************/

LRESULT CALLBACK BasicWindow::Window::wndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) { 
	DWORD mousePosition = lParam; // For mouse-related events. 
	int x = (mousePosition & 0xFFFF); 
	int y = (mousePosition >> 16); 
	DWORD error = GetLastError (); 
	std::wstringstream text; 
	PAINTSTRUCT ps; 
	HDC hdc; 
	switch (uMsg) { 
		case WM_NCCREATE: 
			onnccreate (); 
			return DefWindowProc (hwnd, uMsg, wParam, lParam); 
		case WM_CREATE: 
			oncreate (); 
			return DefWindowProc (hwnd, uMsg, wParam, lParam); 
		case WM_COMMAND: 
			oncommand ((wParam>>16), (wParam & 0xFFFF)); 
			break; 
		case WM_KEYDOWN: 
			onkeydown ((wchar_t)wParam); 
			break; 
		case WM_KEYUP: 
			onkeyup ((wchar_t)wParam); 
			break; 
		case WM_CHAR: 
			onchar ((wchar_t)wParam); 
			break; 
		case WM_MOUSEMOVE: 
			onmousemove (x, y); 
			break; 
		case WM_LBUTTONDOWN: 
			onmousedown (x, y); 
			break; 
		case WM_LBUTTONUP: 
			onmouseup (x, y); 
			break; 
		case WM_RBUTTONDOWN: 
			onrightdown (x, y); 
			break; 
		case WM_RBUTTONUP: 
			onrightup (x, y); 
			break; 
		case WM_MOVE: 
			onmove (x, y); 
			break; 
		case WM_SIZE: 
			onresize (x, y); 
			break; 
		case WM_PAINT: 
			hdc= BeginPaint (hwnd, &ps); 
			drawWin32 (hdc); 
			EndPaint (hwnd, &ps); 
			break; 
		case WM_CLOSE: 
			if (requestClose ()) return DefWindowProc (hwnd, uMsg, wParam, lParam); 
			break; 
		case WM_NCDESTROY: 
			text.str (L""); 
			text << "WM_NCDESTROY; error code: " << error << std::endl; 
			MessageBox (0, text.str ().c_str (), 0, 0); 
			break; 
		case WM_DESTROY: 
			ondestroy (); 
			PostQuitMessage (0); 
			hWnd = NULL; 
			break; 
		default: 
			return DefWindowProc (hwnd, uMsg, wParam, lParam); 
	} 
	return 0; 
} 
LRESULT CALLBACK BasicWindow::Window::staticWndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) { 
	Window * win; 
	if (uMsg == WM_NCCREATE) { 
		win = reinterpret_cast<Window *> (((LPCREATESTRUCT)lParam)->lpCreateParams); 
		SetWindowLongPtr (hwnd, GWLP_USERDATA, (LONG_PTR)win); 
		win->hWnd = hwnd; 
	} else { 
		win = reinterpret_cast<Window *> (GetWindowLongPtr (hwnd, GWLP_USERDATA)); 
	} 
	return win->wndProc (hwnd, uMsg, wParam, lParam); 
} 

void BasicWindow::Window::setText (std::wstring text) { 
	wWinText = text; 
	if (hWnd) SetWindowText (hWnd, text.c_str ()); 
} 
std::wstring BasicWindow::Window::getText () { 
	if (!hWnd) return wWinText; 
	std::wstring text; 
	int len = GetWindowTextLength (hWnd); 
	wchar_t * p = new wchar_t [len + 1]; 
	GetWindowText (hWnd, p, len); 
	text = p; 
	delete p; 
	return text; 
} 

void BasicWindow::Window::setPosition (int x, int y) { 
	position.x = x; 
	position.y = y; 
	if (hWnd) SetWindowPos (hWnd, NULL, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE); 
} 
void BasicWindow::Window::setSize (int w, int h) { 
	position.w = w; 
	position.h = h; 
	if (hWnd) SetWindowPos (hWnd, NULL, 0, 0, w, h, SWP_NOZORDER | SWP_NOMOVE);
} 
Box BasicWindow::Window::getWindowBox () {
	RECT r; 
	Box win; 
	if (!hWnd) { 
		win.note = L"Pre-Window Estimate"; 
		win.x = position.x; 
		win.y = position.y; 
		win.w = position.w; 
		win.h = position.h; 
	} 
	if (!GetWindowRect (hWnd, &r)) { 
		win.success = false; 
		return win; 
	} 
	win.x = r.left; 
	win.y = r.top; 
	win.w = r.right - r.left; 
	win.h = r.bottom - r.top; 
	return win; 
}
Box BasicWindow::Window::getClientBox () {
	RECT r; 
	Box client; 
	if (!hWnd || !GetClientRect (hWnd, &r)) { 
		client.success = false; 
		if (hWnd) client.error = GetLastError (); 
		return client; 
	} 
	client.x = r.left; 
	client.y = r.top; 
	client.w = r.right - r.left; 
	client.h = r.bottom - r.top; 
	return client; 
} 

void BasicWindow::Window::show (std::wstring how) { 
	if (!hWnd) { 
		showString = how; 
		return; 
	} 
	std::wstringstream ss; 
	std::wofstream ofs ("win.txt", std::ios::out | std::ios::app); 
	ss << "Showing window (" << how << ") ... " << std::endl; 
	ofs << ss.str (); 
	if (how == L"full" || how == L"fullscreen") { 
		showString = L"full"; 

	} else { 
		if (showString == L"full") {

		} 
		if (how == L"default") { 
			ShowWindow (hWnd, CmdShow); 
		} else if (how == L"normal") {
			ShowWindow (hWnd, SW_SHOWNORMAL); 
		} else if (how == L"hidden" || how == L"hide") {
			ShowWindow (hWnd, SW_HIDE); 
		} else if (how == L"max") {
			ShowWindow (hWnd, SW_MAXIMIZE); 
		} else if (how == L"min") { 
			ShowWindow (hWnd, SW_MINIMIZE); 
		} else { 
			ShowWindow (hWnd, SW_RESTORE); 
		} 
		showString = how; 
	} 
	ofs << "Done. " << std::endl; 
} 
void BasicWindow::Window::show (std::string how) { 
	std::wstring text; 
	unsigned int i; 
	for (i = 0; i < how.size (); i++) text.push_back ((wchar_t)how[i]); 
	show (text); 
} 
void BasicWindow::Window::show (const wchar_t * how) { 
	std::wstring howString = how; 
	show (howString); 
} 
void BasicWindow::Window::show (const char * how) { 
	std::string howString = how; 
	show (howString); 
} 
void BasicWindow::Window::show (bool whether) { 
	if (whether) show (L""); 
	else show (L"hidden"); 
} 

void BasicWindow::Window::destroy () { 
	unsigned int i; 
	if (closed) return; 
	for (i = 0; i < children.size (); i++) children[i]->destroy (); 
	if (hWnd) DestroyWindow (hWnd); 
	UnregisterClass (wc.lpszClassName, hInst); 
	closed = true; 
} 

