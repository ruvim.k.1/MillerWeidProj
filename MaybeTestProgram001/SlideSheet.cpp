#include "include/MemExpWindow.hpp" 

namespace MemExp { 
	namespace SlideTypes { 
		Command SingleCenteredWord::wordCommand (TypeCentered); 
		Command DoubleCenteredWord::wordUpper (TypeDoubleCenteredUpper); 
		Command DoubleCenteredWord::wordLower (TypeDoubleCenteredLower); 
		Command TwoWordsOnTwoSides::leftWord (TypeLeftmost); 
		Command TwoWordsOnTwoSides::rightWord (TypeRightmost); 
	} 
} 

namespace MemExp { 
	size_t SlideSheet::textCount = 0; 
	SlideSheet::SlideSheet () : duration (DURATION_INFINITE) {} 
	SlideTypes::SingleCenteredWord::SingleCenteredWord () : wordColor (255, 255, 255, 255) {} 
	SlideTypes::TwoWordsOnTwoSides::TwoWordsOnTwoSides () : 
		clrWord1 (255, 255, 255, 255), 
		clrWord2 (255, 255, 255, 255) {}
	void SlideTypes::SingleCenteredWord::setWord (std::wstring text) { wordText = text; } 
	void SlideTypes::SingleCenteredWord::setWord (Color color) { wordColor = color; } 
	void SlideTypes::TwoWordsOnTwoSides::setLeft (std::wstring text) { txtWord1 = text; } 
	void SlideTypes::TwoWordsOnTwoSides::setLeft (Color color) { clrWord1 = color; } 
	void SlideTypes::TwoWordsOnTwoSides::setRight (std::wstring text) { txtWord2 = text; } 
	void SlideTypes::TwoWordsOnTwoSides::setRight (Color color) { clrWord2 = color; } 
	void SlideTypes::SingleCenteredWord::setFontFace (std::wstring fontFace) { wordCommand.fontFace = fontFace; } 
	void SlideTypes::SingleCenteredWord::setFontSize (unsigned int fontSize) { wordCommand.fontSize = fontSize; } 
	void SlideTypes::DoubleCenteredWord::setFontFace (std::wstring fontFace) {
		wordUpper.fontFace = fontFace; 
		wordLower.fontFace = fontFace; 
	} 
	void SlideTypes::DoubleCenteredWord::setFontSize (unsigned int fontSize) {
		wordUpper.fontSize = fontSize; 
		wordLower.fontSize = fontSize; 
	} 
	void SlideTypes::TwoWordsOnTwoSides::setFontFace (std::wstring fontFace) {
		leftWord.fontFace = fontFace; 
		rightWord.fontFace = fontFace; 
	} 
	void SlideTypes::TwoWordsOnTwoSides::setFontSize (unsigned int fontSize) {
		leftWord.fontSize = fontSize; 
		rightWord.fontSize = fontSize; 
	} 
	Command & SlideTypes::SingleCenteredWord::command () { return wordCommand; } 
	Command & SlideTypes::TwoWordsOnTwoSides::left () { return leftWord; } 
	Command & SlideTypes::TwoWordsOnTwoSides::right () { return rightWord; } 
	void SlideTypes::SingleCenteredWord::win32draw (HDC hdc, Box client) const { 
		Command cmd = wordCommand; 
			cmd.text = wordText; 
			cmd.color = wordColor; 
		cmd.win32draw (hdc, client); 
	} 
	void SlideTypes::DoubleCenteredWord::win32draw (HDC hdc, Box client) const { 
		Command cmd1 = wordUpper; 
		Command cmd2 = wordLower; 
			cmd1.text = cmd2.text = wordText; 
			cmd1.color = cmd2.color = wordColor; 
		cmd1.win32draw (hdc, client); 
		cmd2.win32draw (hdc, client); 
	} 
	void SlideTypes::TwoWordsOnTwoSides::win32draw (HDC hdc, Box client) const { 
		Command lWord = leftWord; 
		Command rWord = rightWord; 
			lWord.text = txtWord1; 
			rWord.text = txtWord2; 
			lWord.color = clrWord1; 
			rWord.color = clrWord2; 
		lWord.win32draw (hdc, client); 
		rWord.win32draw (hdc, client); 
	} 
	std::wstring SlideSheet::getText (std::wstring text) const { 
		size_t number = ++textCount; 
		std::wstringstream ss; 
		ss << "text" << number; 
		std::wstring filename = ss.str (); 
		std::wofstream output (filename); 
		output << text; 
		return L"<" + filename + L">"; 
	} 
	std::wstring SlideTypes::SingleCenteredWord::toString () const { 
		std::wstringstream ss; 

		ss << "Single-Text Slide (duration: " << duration << " s; color: " << 
			wordColor.toString () << "; text: [" << getText (wordText) << "]); "; 
		return ss.str (); 
	} 
	std::wstring SlideTypes::TwoWordsOnTwoSides::toString () const { 
		std::wstringstream ss; 
		ss << "Two-Word Slide (duration: " << duration << " s; left: { color: " << 
			clrWord1.toString () << "; text: [" << getText (txtWord1) << "]; }; right: { color: " << 
			clrWord2.toString () << "; text: [" << getText (txtWord2) << "]; }; ); "; 
		return ss.str (); 
	} 
} 
