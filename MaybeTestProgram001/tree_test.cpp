#include <iostream> 

#include <stdlib.h> 
#include <vector> 

#include "NewTree.hpp" 

int main (int argc, char * argv []){ 
	NewTree<double> tree; 
	std::vector<double> arr; 
	unsigned int i; 
	double a; 
	for (i= 0; i < 16; i++){ 
		a= rand (); 
		std::cout << "Pushing " << a << " ... " << std::endl; 
		tree.push (a, a); 
	} 
	std::cout << std::endl; 
	std::cout << "Tree contents: " << std::endl; 
	while (tree.size ()){ 
		a= tree.pop (arr); 
		std::cout << a << std::endl; 
	} 
	std::cout << std::endl; 
	std::cout << "Sorted array: " << std::endl; 
	for (i= 0; i < arr.size (); i++) std::cout << arr[i] << std::endl; 
} 