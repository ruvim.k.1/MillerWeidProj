#include "NTree.hpp" 

NTree::NTree (){ 
	init (2); 
} 
NTree::NTree (unsigned int nbranches){ 
	init (nbranches); 
} 
NTree::NTree (NTree & from){ 
	nsub = from.nsub; 
	sortDirection = from.sortDirection; 
	heapSize = from.heapSize; 
	objects = from.objects; 
	weights = from.weights; 
	zero_pointer = nullptr; 
	zero_weight = 0; 
} 
void NTree::init (unsigned int n){ 
	if (n) nsub = n; 
	else nsub = 2; 
	sortDirection = -1; 
	resize (0); 
	zero_pointer = nullptr; 
	zero_weight = 0; 
} 
unsigned int NTree::parent (unsigned int index) const { 
	return (index + nsub - 2) / nsub; 
} 
unsigned int NTree::child (unsigned int parent, unsigned int nchild) const { 
	return (nsub * (parent - 1) + 2) + nchild; 
} 
double & NTree::weight (unsigned int index) { 
	if (index >= weights.size ()) { 
		return zero_weight; 
	} 
	return weights[index]; 
} 
std::shared_ptr<NTreeObject> & NTree::access (unsigned int index){ 
	if (index >= objects.size ()) { 
		return zero_pointer; 
	} 
	return objects[index]; 
} 
double NTree::getWeight (unsigned int index) const { 
	if (index >= weights.size ()) return 0; 
	return weights[index]; 
} 
std::shared_ptr<NTreeObject> NTree::getObject (unsigned int index) const { 
	if (index >= objects.size ()) return nullptr; 
	return objects[index]; 
} 
bool NTree::needSwap (unsigned int lower, unsigned int higher) const { 
	if (!exists (lower)) return false; 
	if (!exists (higher)) return false; 
	return ((sortDirection < -1) ? (getWeight (lower) < getWeight (higher)): (getWeight (lower) > getWeight (higher))); 
} 
void NTree::swap (unsigned int a, unsigned int b){ 
	std::shared_ptr<NTreeObject> obj = access (a); 
	double wt = weight (a); 
	access (a) = access (b); 
	weight (a) = weight (b); 
	access (b) = obj; 
	weight (b) = wt; 
} 
bool NTree::exists (unsigned int index) const { 
	if (index && index <= heapSize) return true; 
	else return false; 
} 
void NTree::heapifyUp (unsigned int start){ 
	unsigned int now = start; 
	unsigned int up = parent (now); 
	while (needSwap (now, up)) { 
		swap (now, up); 
		now = up; 
		up = parent (now); 
	} 
} 
void NTree::heapify (unsigned int start){ 
	unsigned int now = start; 
	unsigned int low = child (now, 0); 
	unsigned int i; 
	while (exists (low)) { 
		for (i = 1; i < nsub; i++) { 
			if (needSwap (child (now, i), low)) low = child (now, i); 
		} 
		if (!needSwap (low, now)) break; 
		swap (now, low); 
		now = low; 
		low = child (now, 0); 
	} 
} 
void NTree::push (double the_weight, std::shared_ptr<NTreeObject> the_object){ 
	resize (heapSize + 1); // Will increment heapSize. 
	weight (heapSize) = the_weight; 
	access (heapSize) = the_object; 
	heapifyUp (heapSize); 
} 
std::shared_ptr<NTreeObject> NTree::pop (){ 
	return pop (nullptr); 
} 
std::shared_ptr<NTreeObject> NTree::pop (bool use_this_overload_to_crash_program){ 
	std::vector<std::shared_ptr<NTreeObject> > fail; 
	return fail[0]; 
} 
std::shared_ptr<NTreeObject> NTree::pop (std::vector<std::shared_ptr<NTreeObject> > & also_save_to){ 
	return pop (&also_save_to); 
} 
std::shared_ptr<NTreeObject> NTree::pop (std::vector<std::shared_ptr<NTreeObject> > * also_save_to){ 
	std::shared_ptr<NTreeObject> pointer = peek_top (); 
	if (!heapSize) return pointer; // End-of-priority-heap reached. 
	if (heapSize > 1) swap (1, heapSize); 
	if (also_save_to) also_save_to->push_back (pointer); 
	resize (heapSize - 1); 
	heapify (1); 
	return pointer; 
} 
std::shared_ptr<NTreeObject> NTree::peek_top () const { 
	return getObject (1); 
} 
std::vector<std::shared_ptr<NTreeObject> > NTree::getUnorderedArray () const { 
	std::vector<std::shared_ptr<NTreeObject> > arr; 
	unsigned int i; 
	arr.reserve (heapSize); 
	for (i = 1; i <= heapSize; i++) arr.push_back (getObject (i)); 
	return arr; 
} 
void NTree::resize (unsigned int need) { 
	objects.resize (1 + need, nullptr); 
	weights.resize (1 + need, 0); 
	heapSize = need; 
} 
unsigned int NTree::size () const { 
	return heapSize; 
} 

