
class LIST::OfStream : public FileStream { 
	public: 
	// Properties: 
	std::wofstream stream; 
	// Methods: 
	OfStream (); 
	OfStream (const wchar_t * filename, std::ios::openmode mode = std::ios::out); 
	void open (const wchar_t * filename, std::ios::openmode mode = std::ios::out); 
}; 
