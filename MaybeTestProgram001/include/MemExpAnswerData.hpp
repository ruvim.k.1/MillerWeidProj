#include "DataMap.hpp" 
// DataMap.hpp has multiple-include guards, so it's okay to include multiple times, if anything. 

class MemExp::AnswerData : public DataMapExtra { 
	public: 
	// Properties: 
		Number clockTime; 
		Number primeTime; 
		Number targetTime; 
		Number answerTime; 
		Number stddevUsed; 
		Number pCorrect; 
		unsigned int condition; 
		Command prime; 
		Command target; 
		Command left; 
		Command right; 
	// Methods: 
		AnswerData (); 
		AnswerData (AnswerData & from); 
		AnswerData (AnswerData * from); 
		virtual ~AnswerData (); 
		virtual std::wstring const getCSV (); // Exports data to a CSV line for one row. 
		virtual bool setCSV (std::wstring row); // Imports data from a CSV row. 
	private: 
		void initialize (AnswerData * from); // AnswerData () constructors should call this. 

}; 
