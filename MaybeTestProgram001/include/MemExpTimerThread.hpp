#pragma once 
#include "Window.hpp" 
#include "NewTree.hpp" 
namespace MemExp { class CommandQueueObject; }; 
//class MemExp::CommandQueueObject : public NTreeObject { 
//	public: 
//	Command cmd; 
//}; 
class MemExp::TimerThread : 
	public BasicWindow::Thread { 
	private: 
	std::chrono::system_clock::duration timerStarted; 
	public: 
	// Properties: 
		Number time; 
		Number lastDuration; 
		std::chrono::duration<std::chrono::system_clock::rep, std::chrono::system_clock::period> startT; 
		unsigned int slidePollInterval; // In milliseconds. 
		BasicWindow::Window * win; 
		std::vector<std::shared_ptr<SlideSheet>> slideQueue; 
		std::shared_ptr<SlideSheet> nowPlaying; 
#ifdef _DEBUG 
#define DEBUG_RETIRED_COMMANDS 1 
		std::vector<std::shared_ptr<SlideSheet>> retired; 
#endif 
		std::wofstream timerLog; 
	// Methods: 
		TimerThread (); 
		TimerThread (BasicWindow::Window * window); 
		virtual ~TimerThread (); 
		double getTime () const; 
		double getLastDuration () const; 
		std::shared_ptr<SlideSheet> getCurrentSlide () const; 
		void addSlide (std::shared_ptr<SlideSheet> slide); 
		void requestRedraw (); 
		int threadEntry (); 
		void close (); 
}; 

