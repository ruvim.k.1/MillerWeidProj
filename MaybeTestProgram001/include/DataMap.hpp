#ifndef DATAMAP 
#define DATAMAP 

#include <memory> 
#include <vector> 
#include <cmath> 

#ifndef PI 
#define PI 3.141592653589793238462643383279502884 
#endif 

class DataMapExtra { 
	public: 
	DataMapExtra (); 
	virtual ~DataMapExtra (); 
}; 

class DataMap { 
	public: 
	// Properties: 
	unsigned int dimension; 
	double discretization; 
	std::vector<std::shared_ptr<DataMapExtra> > extras; 
	std::vector<std::vector<double> > points; 
	std::vector<std::vector<double> > stddev; 
	std::vector<double> samples; 
	// Methods: 
	DataMap (); 
	virtual ~DataMap (); 
	double P (std::vector<double> probe, double & prob_of_being_accurate); 
	double P (std::vector<double> probe); // Probability of correct answer when in probe region of data space. 
	std::vector<double> delP (std::vector<double> probe); 
	std::vector<double> find (double likelihood, double error, std::vector<double> init_x0); 
	std::vector<double> find (double likelihood, double error = 0.125); 
	std::vector<double> averageX (unsigned int from = 0, unsigned int to = 0); 
	double average (unsigned int from = 0, unsigned int to = 0); 
	void addSample (std::vector<double> point, std::vector<double> std_dev, double sample); 
	void addSample (std::vector<double> point, std::vector<double> std_dev, double sample, std::shared_ptr<DataMapExtra> extra); 
	static double normpdf (double x, double mean = 0, double stddev = 1); 
}; 

#endif 