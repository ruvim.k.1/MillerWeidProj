#ifndef COUNT_RANDOMIZER 
#define COUNT_RANDOMIZER 

#include <string> 

#include <chrono> 
#include <random> 

namespace CountRandomizerNS { 
	extern std::default_random_engine generator; 
}; 

class CountRandomizer { 
	public: 
	// Properties: 
		int above; // Number to "temporarily" add to each stack when randomizing and at least one stack is above 0. Default is 0. 
		int below; // Number by which the minimum should be likely when all stacks are 0 or below. Default is 0. 
		std::vector<int> stacks; 
	// Methods: 
		CountRandomizer (); 
		std::wstring getCSV (); // Export stacks to CSV string. 
		int fromCSV (std::wstring csv); // Import stacks from CSV string. Returns number of stacks imported. 
		int count (); // Total remaining (sum of all stacks). 
		int & operator [] (unsigned int index); // Stack remaining (for a particular stack). 
		void push_back (int stack); 
		void push_back (const std::vector<int> & some_stacks); 
		void inc_stack (unsigned int index); // Increments a stack's count, opposite of inc_stack (). 
		void dec_stack (unsigned int index); // Decrements a stack's count, as if a random () has been drawn from that stack. 
		void resize (unsigned int now); 
		unsigned int size (); // Number of stacks. 
		unsigned int stackP (unsigned int index); // Like [], but weight of a single stack including 'above' and 'below' . 
		unsigned int countP (); // Like count (), but instead returns total including 'above' and 'below' . 
		unsigned int random (); // Select weighted random stack (weight = stack's count). Updates the selected stack by decrementing its count. 
		unsigned int random (bool update_stack); // Same as random (), but here get to choose whether to "peek" (update = false) or "pop" (update = true). 
}; 

#endif 
