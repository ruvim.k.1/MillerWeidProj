#pragma once
#include "windows.h" 
#include <string> 
#include <fstream> 
#include <iostream> 
#include <memory> 
#include <vector> 

class Box; 

namespace BasicWindow {
	class Window; 
	class Thread; 
	extern unsigned int counter; 
}; 

#include "Box.hpp" 

class BasicWindow::Window {
	public: 
	// Properties and Variables: 
		HINSTANCE hInst; 
		HINSTANCE hPrevInst; 
		LPWSTR CmdLine; 
		int CmdShow; 
		HWND hWnd; 
		WNDCLASSEX wc; 
		MSG msg; 
		Box position; 
		std::wstring wWinText; 
		std::wstring wClassName; 
		std::wstring showString; 
		Window * parent; 
		std::vector<std::shared_ptr<Window> > children; 
	// Methods: 
		Window (); 
		virtual ~Window (); 
		int messageLoop (); 
		virtual void onnccreate (); 
		virtual void oncreate (); 
		virtual void onkeydown (wchar_t keyCode); 
		virtual void onkeyup (wchar_t keyCode); 
		virtual void onchar (wchar_t keyCode); 
		virtual void onmousedown (int clientX, int clientY); 
		virtual void onmousemove (int clientX, int clientY); 
		virtual void onmouseup (int clientX, int clientY); 
		virtual void onrightdown (int clientX, int clientY); 
		virtual void onrightup (int clientX, int clientY); 
		virtual void oncommand (int type, int id); 
		virtual void onmove (int x, int y); 
		virtual void onresize (int x, int y); 
		virtual void ondestroy (); 
		virtual void drawWin32 (HDC hdc); 
		virtual bool requestClose (); 
		virtual void close (); 
		LRESULT CALLBACK wndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam); 
		static LRESULT CALLBACK staticWndProc (HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam); 
		void setText (std::wstring text); 
		std::wstring getText (); 
		void setPosition (int x, int y); 
		void setSize (int w, int h); 
		Box getWindowBox (); 
		Box getClientBox (); 
		void show (std::wstring how); 
		void show (std::string how); 
		void show (const wchar_t * how); 
		void show (const char * how); 
		void show (bool whether); 
		void destroy (); 
	private: 
	bool closed; 
}; 

#include "Thread.hpp" 

