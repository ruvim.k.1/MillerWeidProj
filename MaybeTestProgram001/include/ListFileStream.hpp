
namespace LIST {
	void fxnCheckBOM (std::wifstream * stream, FileStream * fs); 
}; 

class LIST::FileStream { 
	public: 
	// Properties: 
	bool bigEndianByteOrder; 
	unsigned char bomSize; 
	unsigned char bitCount; 
	// Methods: 
	FileStream (); 
	virtual void open (const wchar_t * filename, std::ios::openmode mode = std::ios::in) = 0; 
	virtual void checkBOM (std::wifstream stream); 
	virtual void checkBOM (std::wofstream stream); 
}; 
