#pragma once
class BasicWindow::Thread {
	public: 
	// Properties: 
	HANDLE hThread; 
	DWORD threadID; 
	std::shared_ptr<Thread> self; 
	bool needClose; 
	// Methods: 
	Thread (); 
	virtual ~Thread (); 
	static DWORD WINAPI static_threadEntry (LPVOID param); 
	virtual int threadEntry (); 
	virtual void close (); 
}; 

