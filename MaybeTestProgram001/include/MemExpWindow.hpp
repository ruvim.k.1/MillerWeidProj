#pragma once
#include "Window.hpp" 
#include "ListTextFile.hpp" 

#include "DataMap.hpp" 

#include "NewTree.hpp" 

#include <chrono> 
#include <random> 
#include <cmath> 

#include <memory> 

#include <pdflib.hpp> 

namespace MemExp { 
	typedef double Number; 
	class Command; 
	class CommandGenerator; 
	class CommandGeneratorStage2; 
	class SlideSheet; 
	class AnswerData; 
	class AnswerDataList; 
	class TimerThread; 
	class Window; 
	class WordData; 
	class QuestImplementation; 
	extern std::default_random_engine generator; 
}; 

#include "QuestImplementation.hpp" 

#include "MemExpCommand.hpp" 
#include "MemExpCommandGenerator.hpp" 
#include "MemExpCommandGeneratorStage2.hpp" 
#include "SlideSheet.hpp" 
#include "MemExpAnswerData.hpp" 
#include "MemExpAnswerDataList.hpp" 
#include "MemExpTimerThread.hpp" 
#include "MemExpWordData.hpp" 
class MemExp::Window :
	public BasicWindow::Window { 
	public: 
	// Properties: 
		std::shared_ptr<TimerThread> timerThread; 
		std::vector<std::shared_ptr<WordData> > lists; 
		std::vector<bool> enabledLists; 
		std::vector<std::shared_ptr<CommandGenerator> > stages; 
		unsigned int stage; 
		std::wofstream log; 
		void * dd_ptr; 
		Number monitor_freq; 
		Number screenWidth; 
		Number screenHeight; 
		Number prevPrimeDuration; 
		Number prevTargetDuration; 
		Number prevAnswerDuration; 
		Number nextAllowedInput; // The time, in TimerThread units, when the next user input is allowed. 
		SettingsReader settings; 
		std::vector<DataMap> dataMap; 
		unsigned int paddingLeft; 
		unsigned int paddingTop; 
		unsigned int paddingRight; 
		unsigned int paddingBottom; 
		unsigned char backgroundR; 
		unsigned char backgroundG; 
		unsigned char backgroundB; 
		unsigned char backgroundA; // Not used. 
		bool alreadyStarted; 
		bool nowWelcoming; 
		bool waitVR; 
	// Methods: 
		Window (); 
		virtual ~Window (); 
		// Interface Methods: 
		void addWordList (std::shared_ptr<WordData> list); 
		void addGenerator (std::shared_ptr<CommandGenerator> gen); 
		void generateCycle (Number delay = 0); 
		void clearActualTimes (); 
		void checkAnswer (Command usrAnswer, bool needPutFeedback = true); 
		DataMap & getDataMap (unsigned int index); 
		unsigned int countDataMaps (); 
		// Log Methods: 
		void writeLog (std::wstring msg); 
		void ddLogFeedback (HRESULT result); 
		// Our methods: 
		void drawWin32 (HDC hdc); 
		void onmouseup (int clientX, int clientY); 
		void onchar (wchar_t keycode); 
}; 

