#include <random> 

class MemExp::CommandGenerator { 
	public: 
	// Properties: 
		// The Quest implementation: 
		QuestImplementation quest; 
		// Times: 
		Number signTime; 
		Number primeTime; 
		Number maskTime; 
		Number targetTime; 
		// Various settings: 
		Number targetTimeA; 
		Number targetTimeB; 
		Number targetTimeNow; 
		Number targetTimeBase; 
		Number targetTimeRange; 
		Number targetTimeFinal; 
		Number maskPlusTarget; 
		Number feedbackTime; 
		Number monitorFreq; 
		Number exactPrimeTime; 
		std::vector<Number> avlPrimeTimes; 
		// Draw and clear times (set by MemExp::Window::drawWin32 () method): 
		Number primeDrawn; 
		Number primeClear; 
		Number targetDrawn; 
		Number targetClear; 
		Number answerDrawn; 
		Number answerClear; 
		// What to display when the answer was correct or incorrect that the user answered: 
		Command msgCorrect; 
		Command msgIncorrect; 
		Command answerTemplate; 
		// The question queue commands (first sign displayed to alert user, then prime, then target, then mask): 
		Command theSign; 
		Command thePrime; 
		Command theTarget; 
		Command theFoil; 
		Command theMask; 
		// The left and right answer options: 
		Command ansLeft; 
		Command ansRight; 
		bool primeNeedMatchTarget; 
		bool primeNeedMatchFoil; 
		// The user answer data entry object smart-pointer: 
		std::shared_ptr<AnswerData> dataEntry; 
		std::shared_ptr<AnswerData> draft; 
		// The answer counts, etc.: 
		unsigned int answerCount; 
		unsigned int desiredLinearTargetCount; 
		// Pointer to a settings object (pointer to be set by MemExp::Window::addGenerator () method): 
		SettingsReader * settings; 
		// The path of the CSV file to save to: 
		std::wstring csvFile; // Blank --> do not save to CSV 
		// The paths of the welcome text files: 
		std::vector<std::wstring> welcomeFiles; 
		std::vector<std::wstring> alreadyAsked; 
		unsigned int currentWelcome; 
		bool welcomeDone; 
	// Methods: 
		CommandGenerator (); 
		static Number getTime (Window * expWindow); 
		static Number timerDelay (Window * expWindow); 
		virtual unsigned int countEntriesInCSV (std::wstring filename); 
		virtual void loadFromCSV (std::wstring filename, DataMap & map); 
		virtual void onaddanswer (AnswerData * answer, DataMap & map); 
		virtual void onaddnewanswer (AnswerData * answer, DataMap & map); 
		virtual void postWelcome (Window * expWindow, Number additional_delay = 0); 
		virtual void putFeedback (Command userAnswer, DataMap & map, Window * expWindow, Number additional_delay = 0); 
		virtual void generate (Window * expWindow, Number additional_delay = 0); 
		virtual void beginGenerateProcedure (); 
		virtual void determineWords (Window * expWindow); 
		virtual void determineTimes (Number time); 
		virtual void determineStyles (); 
		virtual void determineAnswerOptions (Window * expWindow); 
		virtual void adjustTargetTime (DataMap & map); 
		virtual std::wstring sampleWord (Window * expWindow); // This method is called from the generate () method. 
		virtual void markAsAsked (std::wstring theWord); 
		virtual void prepTime (Window * expWindow, DataMap & map, CommandGenerator * prev = nullptr); // Called when switching over to this generator. 
		virtual void wrapUp (Window * expWindow, DataMap & map); // Called before going on to the next generator. 
		virtual bool done (); 
		static void alignLeft (Command & cmd); 
		static void alignRight (Command & cmd); 
		static void alignMiddle (Command & cmd); 
		static void alignTop (Command & cmd); 
		static void alignBot (Command & cmd); 
		static void alignAuto (std::vector<Command> & arr); 
}; 
