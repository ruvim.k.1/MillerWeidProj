
#define DURATION_INFINITE                       (-1) 

namespace MemExp { 
	namespace SlideTypes { 
		class SingleCenteredWord; 
		class DoubleCenteredWord; 
		class TwoWordsOnTwoSides; 
	} 
} 

class MemExp::SlideSheet { 
	// Blank template class. 
	static size_t textCount; 
public: 
	double duration; // How long to display the slide for, in seconds. 
	virtual void win32draw (HDC hdc, Box client) const = 0; 
	virtual std::wstring toString () const = 0; 
	virtual std::wstring getText (std::wstring text) const; 
	SlideSheet (); 
}; 

class MemExp::SlideTypes::SingleCenteredWord : public SlideSheet { 
protected: 
	std::wstring wordText; 
	Color wordColor; 
	static Command wordCommand; 
public: 
	void win32draw (HDC hdc, Box client) const override; 
	std::wstring toString () const override; 
	virtual void setWord (std::wstring text); 
	virtual void setWord (Color color); 
	virtual Command & command (); 
	SingleCenteredWord (); 
	// Static: 
	static void setFontFace (std::wstring fontFace); 
	static void setFontSize (unsigned int fontSize); 
}; 
class MemExp::SlideTypes::DoubleCenteredWord : public SingleCenteredWord { 
protected: 
	static Command wordUpper; 
	static Command wordLower; 
public: 
	void win32draw (HDC hdc, Box client) const override; 
	// Static: 
	static void setFontFace (std::wstring fontFace); 
	static void setFontSize (unsigned int fontSize); 
}; 
class MemExp::SlideTypes::TwoWordsOnTwoSides : public SlideSheet { 
protected: 
	std::wstring txtWord1; 
	std::wstring txtWord2; 
	Color clrWord1; 
	Color clrWord2; 
	static Command leftWord; 
	static Command rightWord; 
public: 
	void win32draw (HDC hdc, Box client) const override; 
	std::wstring toString () const override; 
	virtual void setLeft (std::wstring left); 
	virtual void setLeft (Color left); 
	virtual void setRight (std::wstring right); 
	virtual void setRight (Color right); 
	virtual Command & left (); 
	virtual Command & right (); 
	TwoWordsOnTwoSides (); 
	// Static: 
	static void setFontFace (std::wstring fontFace); 
	static void setFontSize (unsigned int fontSize); 
}; 
