
class LIST::IfStream : public FileStream { 
	public: 
	// Properties: 
	std::wifstream stream; 
	unsigned long long fileSize; 
	// Methods: 
	IfStream (); 
	IfStream (const wchar_t * filename, std::ios::openmode mode = std::ios::in); 
	void open (const wchar_t * filename, std::ios::openmode mode = std::ios::in); 
	std::wstring getLine (); 
	wchar_t getWide (); 
	bool canRead (); 
}; 