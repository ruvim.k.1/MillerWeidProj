#include "ListTextFile.hpp" 

namespace MemExp { 
	namespace List { 
		class AnswerDataHead; 
		class AnswerDataItem; 
	}; 
}; 

class MemExp::List::AnswerDataHead : public LIST::Head { 
	public: 
	std::vector<std::wstring> head; 
}; 
class MemExp::List::AnswerDataItem : public LIST::Item { 
	public: 
	MemExp::AnswerData data; 
}; 

class MemExp::AnswerDataList : public LIST::DataFile { 
	public: 
	// Properties: 
		
	// Methods: 
		std::shared_ptr<LIST::Head> readHead (LIST::IfStream * file); 
		std::shared_ptr<LIST::Item> readItem (LIST::IfStream * file); 
		void writeHead (LIST::OfStream * file, LIST::Head * head); 
		void writeItem (LIST::OfStream * file, LIST::Item * item); 
}; 

