
class LIST::Head {
	public: 
	// Properties: 
		unsigned long long ofsFirstItem; 
		unsigned long long itemCount; 
		std::wstring headerText; 
		std::wstring headerTextTemplate; 
	// Methods: 
		Head (); 
		virtual void update (std::vector<std::shared_ptr<Item> > & list_vector); 
		virtual bool read (IfStream * file); 
		virtual unsigned long long write (); // Use this to count the number of bytes required for the header. 
		virtual unsigned long long write (OfStream * file); // Returns number of bytes written. 
}; 
