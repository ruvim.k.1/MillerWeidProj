#ifndef PRE_EXP_WINDOW 
#define PRE_EXP_WINDOW 

#include <windows.h> 

#include "Window.hpp" 

#include "SettingsReader.hpp" 

namespace PreExp {
	class Window; 
}; 

class PreExp::Window : public BasicWindow::Window { 
	public: 
	// Properties: 
		SettingsReader settings; 
		std::vector<HWND> inputs; 
		std::vector<HWND> buttons; 
	// Methods: 
		Window (); 
		~Window (); 
		// Overrides: 
		void oncreate (); 
		void ondestroy (); 
		void onkeyup (wchar_t keycode); 
		void onresize (int w, int h); 
		void oncommand (int type, int id); 
		void drawWin32 (HDC hdc); 
		// Ours: 
		void movesize (); 
		std::wstring forceAlphanumeric (); 
}; 

#endif 
