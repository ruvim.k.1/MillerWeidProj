#ifndef LIST_INCLUDED 
#define LIST_INCLUDED 

#include <locale> 

#include <sstream> 
#include <fstream> 
#include <memory> 
#include <vector> 
#include <string> 

namespace LIST { 
	class FileStream; 
	class IfStream; 
	class OfStream; 
	class DataFile; 
	class Head; 
	class Item; 
	class SettingsReader; 
}; 

#include "ListFileStream.hpp" 
#include "ListIfStream.hpp" 
#include "ListOfStream.hpp" 

#include "ListHead.hpp" 
#include "ListItem.hpp" 

#include "SettingsReader.hpp" 

class LIST::DataFile { 
	public: 
	// Properties: 
		std::shared_ptr<Head> header; 
		std::vector<std::shared_ptr<Item> > items; 
		unsigned long long wasItemCount; // Number of items (supposed to be). 
		unsigned long long readItemCount; // Number of items successfully read. 
	// Methods: 
		// Constructors and Maybe Destructor: 
			DataFile (); 
			DataFile (std::wstring filename); 
		// Load and save (general, high-level): 
			virtual void load (IfStream * file); 
			virtual void save (OfStream * file); 
			virtual void load (std::wstring file_path); 
			virtual void save (std::wstring file_path); 
		// Specific loading and saving mini-procedures: 
			virtual std::shared_ptr<IfStream> openForReading (std::wstring path); 
			virtual std::shared_ptr<OfStream> openForWriting (std::wstring path); 
			virtual std::shared_ptr<Head> readHead (IfStream * file); 
			virtual std::shared_ptr<Item> readItem (IfStream * file); 
			virtual void writeHead (OfStream * file, Head * head); 
			virtual void writeItem (OfStream * file, Item * item); 
		// Other: 
			virtual void updateHead (); 
		// Interface / Access: 
			virtual unsigned int const size (); 
			virtual std::shared_ptr<Item> & at (unsigned int index); 
			virtual std::shared_ptr<Item> & operator [] (unsigned int index); 
}; 

#endif 