#pragma once 
namespace MemExp { 
#define AlignLeft   DT_LEFT 
#define AlignCenter DT_CENTER 
#define AlignRight  DT_RIGHT 

#define AlignTop    DT_TOP 
#define AlignMiddle DT_VCENTER 
#define AlignBottom DT_BOTTOM 

#define WordBreak   DT_WORDBREAK 
#define Singleline  DT_SINGLELINE 
#define Multiline   0 

#define TypeCentered                        1 
#define TypeDoubleCenteredUpper             2 
#define TypeDoubleCenteredLower             3 
#define TypeLeftmost                        4 
#define TypeRightmost                       5 

	typedef struct _ourRgba { 
		unsigned char r; 
		unsigned char g; 
		unsigned char b; 
		unsigned char a; // Unused as of now. 
		_ourRgba (unsigned int theR, unsigned int theG,
				  unsigned int theB, unsigned int theA) { 
			r = theR; g = theG; b = theB; a = theA; 
		} 
		std::wstring toString () const { 
			std::wstringstream ss; 
			ss << "RGBA(" << r << ", " << g << ", " << b << ", " << a << ")"; 
			return ss.str (); 
		} 
	} Color, RGBA; 
}; 

class MemExp::Command { 
	public: 
	// Properties: 
		std::wstring type; // For miscellaneous purposes, really. In particular, made this 'type' to tell whether it is a dummy answer or not, for the putFeedback () method. 
		std::wstring text; 
		std::wstring fontFace; 
		unsigned int fontSize; 
		unsigned int hAlign; 
		unsigned int vAlign; 
		unsigned int textType; 
		Color color; 
		Number left; 
		Number top; 
		Number right; 
		Number bottom; 
		bool bold; 
		bool italic; 
		bool underline; 
		bool strikethrough; 
	// Methods: 
		Command (); 
		Command (int initFillType /* Use the TypeCentered, etc., Type* constants */); 
		virtual ~Command (); 
		void copyFrom (Command & from); 
		std::wstring toString (); 
		void win32draw (HDC hdc, Box client); 
		void setColor (unsigned char r, unsigned char g, unsigned char b); 
		void setColor (Color theColor); 
}; 

