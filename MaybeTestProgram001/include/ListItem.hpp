
class LIST::Item { 
	public: 
	// Properties: 
		unsigned long long bufferSize; 
		std::wstring value; 
	// Methods: 
		virtual bool read (IfStream * file); 
		virtual unsigned long long write (); 
		virtual unsigned long long write (OfStream * file); 
}; 
