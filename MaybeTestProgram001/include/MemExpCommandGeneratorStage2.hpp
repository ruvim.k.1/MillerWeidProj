#include "CountRandomizer.hpp" 

class MemExp::CommandGeneratorStage2 : public CommandGenerator { 
	public: 
	// Properties: 
		CountRandomizer condition; 
		unsigned int selected; 
	// Methods: 
		CommandGeneratorStage2 (); 
		void beginGenerateProcedure (); 
		void adjustTargetTime (DataMap & map); 
		void onaddanswer (AnswerData * answer, DataMap & map); 
		void onaddnewanswer (AnswerData * answer, DataMap & map); 
		void prepTime (Window * expWindow, DataMap & map, CommandGenerator * prev = nullptr); 
		void wrapUp (Window * expWindow, DataMap & map); 
		bool done (); 
}; 

