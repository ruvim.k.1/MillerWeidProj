#ifndef SETTINGS_READER 
#define SETTINGS_READER 

#include <string> 
#include <fstream> 
#include <unordered_map> 

class SettingsReader { 
	public: 
	// Properties: 
	std::unordered_map<std::wstring, std::wstring> strParams; 
	std::unordered_map<std::wstring, int>		   intParams; 
	std::unordered_map<std::wstring, double>	   dblParams; 
	// Methods: 
	void fromTextFile (std::wstring filename); 
	void dumpAll (std::wofstream & log_file); 
}; 

#endif 
