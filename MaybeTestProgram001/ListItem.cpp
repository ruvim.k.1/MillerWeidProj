#include "include/ListTextFile.hpp" 

bool LIST::Item::read (IfStream * file) { 
	//wchar_t c; 
	std::wstringstream ss; 
	value = file->getLine (); 
	bufferSize = value.size (); 
	// That's it! 
	return true; 
} 
unsigned long long LIST::Item::write () { 
	return value.size (); 
} 
unsigned long long LIST::Item::write (OfStream * file) { 
	unsigned long long was = file->stream.tellp (); 
	file->stream << value; 
	return ((unsigned long long)(file->stream.tellp ()) - was); 
} 
