#include <MemExpWindow.hpp> 

#define F_T__X_BEGIN     (-2.41)  // The "x" parameter at which psi (x) = 0.75 (so 75%) 


MemExp::QuestImplementation::QuestImplementation () : QuestImplementation (0.75) { 

} 
MemExp::QuestImplementation::QuestImplementation (double arg_wantP) { 
	// Default parameter setup: 
	gamma = 0.5; 
	beta = 3.5; 
	eps = 1.5; 
	// Psi-Shape-Related: 
	wantP = arg_wantP; 
	x0 = 0; 
	{ 
		double x; 
		double wantX = -16; 
		double wantY = psi (wantX); 
		double haveY; 
		for (x = -16 + search_dx; x < 16; x += search_dx) { 
			haveY = psi (x); 
			if (std::abs (haveY - wantP) >= std::abs (wantY - wantP)) continue; 
			wantX = x; 
			wantY = haveY; 
		} 
		x0 = wantX; 
	} 
	// Stopping Criteria: 
	chiSqAlpha = 0.95; 
	chiSqDF = 1; 
} 
MemExp::QuestImplementation::~QuestImplementation () { 

} 

double MemExp::QuestImplementation::psi (double actual_x) const { 
	double x = 16 * (actual_x + x0); 
	// For now, we will use the Weibull psychometric function: 
	// Cap the function: 
	if (x < -16.0) return gamma; 
	if (x >   4.0) return 0.99; 
	// For between the caps, do this: 
	return 1 - (1 - gamma) * std::exp (-1 * std::pow (10, (beta / 20) * (x + eps))); 
} 
double MemExp::QuestImplementation::f_T (double x) const { 
	return 4 * normpdf (x, 0.05, 1); // M = 50 ms, SE = 2, but we want to replicate, so not that much certainty. 
	// mu is the probable threshold 
	// sigma is the uncertainty in that threshold 
} 

unsigned int MemExp::QuestImplementation::countHypotheses (double hypothesis_size_dx, double minX, double maxX, int n) const { 
	unsigned int hCount = 0; 
	double L_like = L (T_like (minX, maxX, n), n); 
	double L_cand; // The L of a candidate T_j. 
	double T_j; 
	double halfChiSq = 0.5 * PDFLIB::r8_chi_pdf (chiSqDF, chiSqAlpha); 
	for (T_j = minX; T_j <= maxX; T_j += hypothesis_size_dx) { 
		L_cand = L (T_j); 
		if (std::abs (L_like - L_cand) >= halfChiSq) continue; 
		hCount++; 
	} 
	return hCount; 
} 
