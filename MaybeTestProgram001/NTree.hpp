#ifndef NTREE 
#define NTREE 
#include <vector> 
#include <memory> 
class NTreeObject {}; // Extend this. 
class NTree { 
	public: 
	// Properties: 
		int sortDirection; 
		unsigned int nsub; 
		unsigned int heapSize; 
		std::vector<double> weights; 
		std::vector<std::shared_ptr<NTreeObject> > objects; 
		std::shared_ptr<NTreeObject> zero_pointer; 
		double zero_weight; 
	// Methods: 
		NTree (); 
		NTree (unsigned int nbranches); 
		NTree (NTree & from); 
		void init (unsigned int n); 
		virtual unsigned int parent (unsigned int index) const; 
		virtual unsigned int child (unsigned int parent, unsigned int nchild) const; 
		virtual double & weight (unsigned int index); 
		virtual double getWeight (unsigned int index) const; 
		virtual std::shared_ptr<NTreeObject> & access (unsigned int index); 
		virtual std::shared_ptr<NTreeObject> getObject (unsigned int index) const; 
		virtual bool needSwap (unsigned int lower, unsigned int higher) const; 
		virtual void swap (unsigned int a, unsigned int b); 
		virtual bool exists (unsigned int index) const; 
		virtual void heapifyUp (unsigned int start); 
		virtual void heapify (unsigned int start); 
		void push (double the_weight, std::shared_ptr<NTreeObject> the_object); 
		std::shared_ptr<NTreeObject> pop (); 
		std::shared_ptr<NTreeObject> pop (bool use_this_overload_to_crash_program); 
		std::shared_ptr<NTreeObject> pop (std::vector<std::shared_ptr<NTreeObject> > & also_save_to); 
		std::shared_ptr<NTreeObject> pop (std::vector<std::shared_ptr<NTreeObject> > * also_save_to); 
		std::shared_ptr<NTreeObject> peek_top () const; 
		std::vector<std::shared_ptr<NTreeObject> > getUnorderedArray () const; 
		void resize (unsigned int need); 
		unsigned int size () const; 
}; 
#endif 