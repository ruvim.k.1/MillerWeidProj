#include "include/CountRandomizer.hpp" 

#include <sstream> 

namespace CountRandomizerNS { 
	std::default_random_engine generator ((unsigned int)(std::chrono::system_clock::now ().time_since_epoch ().count ())); 
}; 

CountRandomizer::CountRandomizer () { 
	above = below = 0; 
} 

std::wstring CountRandomizer::getCSV () { 
	std::wstring text; 
	unsigned int i; 
	text = L""; 
	for (i = 0; i < stacks.size (); i++) { 
		if (i) text += L","; 
		text += stacks[i]; 
	} 
	return text; 
} 
int CountRandomizer::fromCSV (std::wstring csv) { 
	unsigned int i = 0; 
	unsigned int j; 
	unsigned int number; 
	std::vector<std::wstring> numbers; 
	std::wstringstream stream; 
	std::wstring text; 
	do {
		text = L""; 
		for (; i < csv.size () && csv.at (i) != ','; i++) text.push_back (csv.at (i)); 
		if (i < csv.size () && csv.at (i) == ',') i++; 
		numbers.push_back (text); 
	} while (i < csv.size ()); 
	stacks.resize (numbers.size ()); 
	for (i = 0, j = 0; i < numbers.size (); i++) { 
		number = -65536; 
		stream.str (L""); 
		stream.clear (); 
		stream.str (numbers[i]); 
		stream >> number; 
		if (number == -65536) continue; 
		stacks[j] = number; 
		j++; 
	} 
	if (stacks.size () > j) stacks.resize (j); 
	return j; 
} 

int CountRandomizer::count () { 
	unsigned int i; 
	int sum = 0; 
	for (i = 0; i < stacks.size (); i++) sum += stacks[i]; 
	return sum; 
} 
int & CountRandomizer:: operator [] (unsigned int index) { 
	if (stacks.size () <= index) stacks.resize (1 + index); 
	return stacks[index]; 
} 
void CountRandomizer::push_back (int stack) { 
	stacks.push_back (stack); 
} 
void CountRandomizer::push_back (const std::vector<int> & some_stacks) { 
	unsigned int i; 
	for (i = 0; i < some_stacks.size (); i++) stacks.push_back (some_stacks[i]); 
} 
void CountRandomizer::inc_stack (unsigned int index) { 
	if (index >= stacks.size ()) stacks.resize (1 + index, 0); 
	stacks[index]++; 
} 
void CountRandomizer::dec_stack (unsigned int index) { 
	if (index >= stacks.size ()) stacks.resize (1 + index, 0); 
	stacks[index]--; 
} 
void CountRandomizer::resize (unsigned int now) { 
	stacks.resize (now); 
} 
unsigned int CountRandomizer::size () { 
	return stacks.size (); 
} 

unsigned int CountRandomizer::stackP (unsigned int index) { 
	if (index >= stacks.size ()) return 0; 
	if (!stacks.size ()) return 0; 
	unsigned int i; 
	unsigned int min = 0; 
	unsigned int max = 0; 
	for (i = 1; i < stacks.size (); i++) { 
		if (stacks[min] > stacks[i]) min = i; 
		if (stacks[max] < stacks[i]) max = i; 
	} 
	if (stacks[max] > 0) { 
		// Use 'above' rule. 
		return above + stacks[index]; 
	} else { 
		// Use 'below' rule. 
		return below + stacks[index] - stacks[min]; 
	} 
} 
unsigned int CountRandomizer::countP () { 
	if (!stacks.size ()) return 0; 
	unsigned int i; 
	unsigned int min = 0; 
	unsigned int max = 0; 
	int sum; 
	for (i = 1; i < stacks.size (); i++) { 
		if (stacks[min] > stacks[i]) min = i; 
		if (stacks[max] < stacks[i]) max = i; 
	} 
	if (stacks[max] > 0) { 
		// Use 'above' rule. 
		sum = stacks.size () * above; 
	} else { 
		// Use 'below' rule. 
		sum = stacks.size () * (below - stacks[min]); 
	} 
	for (i = 0; i < stacks.size (); i++) sum += stacks[i]; 
	return sum; 
} 

unsigned int CountRandomizer::random () { 
	return random (true); 
} 
unsigned int CountRandomizer::random (bool update_stack) { 
	std::uniform_real_distribution<double> dist (0, countP ()); 
	double number = dist (CountRandomizerNS::generator); 
	unsigned int target = (unsigned int)std::floor (number); 
	unsigned int i; 
	unsigned int p; 
	unsigned int sum = 0; 
	for (i = 0; i < stacks.size (); i++) { 
		p = stackP (i); 
		if (sum + p > target) break; 
		sum += p; 
	} 
	if (i >= stacks.size ()) i = stacks.size () - 1; 
	if (update_stack) dec_stack (i); // Essentially same as:  stacks[i]--; 
	return i; 
} 

