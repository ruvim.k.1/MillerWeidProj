#ifndef N_aryTree_IS_DEFINED 
#define N_aryTree_IS_DEFINED 1 
// The 'ifndef' statement is there in order to check for more-than-once inclusion of this file, which is totally 
// okay with the standard C includes, so why not make it totally okay with our library as well? 
#include <vector> 
class N_aryTree_Number { 
	public: 
	double value= 0; 
	N_aryTree_Number (){ 
		
	} 
	N_aryTree_Number (double x){ 
		value= x; 
	} 
	N_aryTree_Number (N_aryTree_Number & from) {
		value = from.value; 
	} 
	N_aryTree_Number (N_aryTree_Number * from) {
		value = from->value; 
	}
}; 
template <typename T> 
class N_aryTree { 
	public: 
		int nsub= 2; // Number of sub-branches on each branch (2 for a "binary" tree). 
		int heap_sort= -1; // Float the lowest weights to the top. 
	private: 
	int heap_size= 0; 
	public: 
		std::vector<T> objects; 
		std::vector<N_aryTree_Number> weights; 
		T dummy; 
	private: 
	virtual int parent (int index){ 
		// Returns the integer index of the parent node. 
		return ((int)((index + nsub - 2) / nsub)); // Automatically rounds down. 
	} 
	virtual int child (int index, int nchild){ 
		// Returns the integer index of the n-th child node (n = 0 for the first child). 
		return (nsub * index - nsub + 2) + nchild; 
	} 
	double weight (unsigned int index){ 
		// Returns the weight of the node at the heap index. 
		if (index < 0) return 0; 
		if (index >= weights.size ()) return 0; 
		//if (!weights[index]) return 0; 
		return weights[index].value; 
	} 
	bool need_swap (int lower, int higher){ 
		// Returns whether a node needs to be swapped with its parent (the node is 'lower' and the parent is 'higher'). 
		return ((heap_sort < 0)? (weight (lower) < weight (higher)): (weight (lower) > weight (higher))); 
	} 
	void swap (int a, int b){ 
		T one; 
		N_aryTree_Number number; 
		one= objects[a]; 
		number= weights[a]; 
		objects[a]= objects[b]; 
		weights[a]= weights[b]; 
		objects[b]= one; 
		weights[b]= number; 
	} 
	bool exists (int index){ 
		if (index && index <= heap_size) return true; // ????????? 
		else return false; 
	} 
	void heapify_up (int start){ 
		int now= start; 
		int up= parent (now); 
		while (up /* > 0 or != NULL */ && need_swap (now, up)){ 
			// Swap. 
			swap (now, up); 
			// Go up one. 
			now= up; 
			up= parent (now); 
		} 
	} 
	void heapify (int start){ 
		int now= start; 
		int index; 
		int down; 
		int i; 
		while (now /* > 0 or != NULL */){ 
			down= child (now, 0); 
			if (!exists (down)) break; 
			for (i= 1; i < nsub; i++){ 
				index= child (now, i); 
				if (!exists (index)){ 
					// Exit the 'for' loop. 
					i= nsub; 
					continue; 
				} 
				if (need_swap (index, down)) down= index; 
			} 
			if (!need_swap (down, now)) break; 
			swap (now, down); 
			now= down; 
		} 
	} 
	public: 
	N_aryTree (){ 
		// 0-th element is empty for priority heap implementation 
		objects.resize (1, dummy); 
		weights.resize (1); 
	} 
	N_aryTree (int n){ 
		// 0-th element is empty for priority heap implementation 
		if (n > 0) nsub= n; 
		objects.resize (1, dummy); 
		weights.resize (1); 
	} 
	N_aryTree (N_aryTree & from) { 
		unsigned int i; 
		n_sub = from.n_sub; 
		heap_sort = from.heap_sort; 
		objects = from.objects; 
		weights = from.weights; 
		dummy = from.dummy; 
	} 
	~N_aryTree () {
		
	}
	int size (){ 
		return heap_size; 
	} 
	void push (double weight, T obj){ 
		if (!heap_size){ 
			// Just add as first element. 
			objects.insert (objects.begin () + 1, obj); 
			weights.insert (weights.begin () + 1, N_aryTree_Number (weight)); 
			heap_size= 1; 
			return; 
		} 
		// Otherwise, use regular branch insertion method. 
		objects.insert (objects.begin () + heap_size + 1, obj); 
		weights.insert (weights.begin () + heap_size + 1, N_aryTree_Number (weight)); 
		heap_size++; 
		heapify_up (heap_size); 
	} 
	T pop (bool opt_delete_old_wt = true){ 
		// Pops the top node off the stack and returns it. 
		T obj= peek_top (); 
		if (!heap_size){ 
			std::cerr << "pop () called when there was no heap data available. " << std::endl; 
			return dummy; 
		} else if (heap_size == 1){ 
			heap_size= 0; 
			return obj; 
		} 
		objects[1]= objects[heap_size]; 
		weights[1]= weights[heap_size]; 
		heap_size--; 
		heapify (1); 
		objects.resize (4 + heap_size); // Leave some padding (the +4). 
		weights.resize (4 + heap_size); 
		return obj; 
	} 
	T peek_top (){ 
		// Returns the top node without popping it. 
		if (heap_size) return objects[1]; 
		return dummy; 
	} 
}; 
#endif 
