#include "include/ListTextFile.hpp" 

void SettingsReader::fromTextFile (std::wstring filename) { 
	wchar_t c = 1; 
	LIST::IfStream stream; 
	std::wstring name; 
	std::wstring valString; 
	std::wstring useless; 
	std::wstringstream ss; 
	bool stop = false; 
	int valInteger; 
	double valDouble; 
	stream.open (filename.c_str ()); 
	stream.stream.seekg (stream.bomSize); 
	while (stream.canRead ()) { 
		name = L""; 
		stop = false; 
		while (stream.canRead () && !stop) { 
			c = stream.getWide (); 
			if (c == 10 || !c) {
				stop = true; 
				continue; 
			}
			if (c == ':') {
				stop = true;
				continue;
			} 
			name.push_back (c); 
		} 
		if (c == 10 || !c) continue; 
		if (dblParams.count (name) > 0) { 
			ss.str (stream.getLine ()); 
			ss >> valDouble; 
			dblParams[name] = valDouble; 
		} else if (intParams.count (name) > 0) { 
			ss.str (stream.getLine ()); 
			ss >> valInteger; 
			intParams[name] = valInteger; 
		} else { 
			std::wstring text = stream.getLine (); 
			unsigned int i = 0; 
			unsigned int j = text.size () - 1; 
			while (i < text.size () && (text.at (i) == ' ' || text.at (i) == '	' || text.at (i) == 13 || text.at (i) == 10)) i++; 
			while (j > i && (text.at (j) == ' ' || text.at (j) == '	' || text.at (j) == 13 || text.at (j) == 10)) j--; 
			stop = false; 
			strParams[name] = text.substr (i, j - i + 1); 
			continue; 
		} 
		stop = false; 
		//useless = stream.getLine (); // Finish the line. 
	} 
} 

void SettingsReader::dumpAll (std::wofstream & log_file) { 
	std::wstring a; 
	std::wstring b; 
	// This method is a little buggy ... 
	log_file << "Settings: " << std::endl; 
	for (auto it = dblParams.cbegin (); it != dblParams.cend (); it++) {
		log_file << "(dbl)["; 
		log_file << it->first.c_str (); 
		log_file << "]: {"; 
		log_file << it->second; 
		log_file << "}; "; 
		log_file << std::endl;
	} 
	for (auto it = intParams.cbegin (); it != intParams.cend (); it++) {
		log_file << "(int)["; 
		log_file << it->first; 
		log_file << "]: {"; 
		log_file << it->second; 
		log_file << "}; "; 
		log_file << std::endl;
	} 
	for (auto it = strParams.cbegin (); it != strParams.cend (); it++) { 
		a = it->first; 
		b = it->second; 
		log_file << "(str)["; 
		log_file << a; 
		log_file << "]: {"; 
		log_file << b; 
		log_file << "}; "; 
		log_file << std::endl; 
	} 
	log_file << std::endl; 
} 
