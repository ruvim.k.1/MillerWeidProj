#include "include/DataMap.hpp" 

DataMapExtra::DataMapExtra () { 

} 
DataMapExtra::~DataMapExtra () { 

} 

DataMap::DataMap () { 
	dimension = 1; 
	discretization = 0.00625; 
} 
DataMap::~DataMap () { 

} 

double DataMap::P (std::vector<double> probe, double & prob_of_being_accurate) {
	unsigned int i, j; 
	double distance; 
	double p; 
	double p_res = 1; 
	double p_acc = 1; 
	std::vector<double> p_sum; 
	std::vector<double> p_val; 
	p_sum.resize (dimension, 0); 
	p_val.resize (dimension, 0); 
	for (i = 0; i < dimension; i++) { 
		for (j = 0; j < samples.size (); j++) { 
			if (i < probe.size ()) distance = points[j][i] - probe[i]; 
			else distance = 0; 
			p = normpdf (distance, 0, stddev[j][i]); 
			p_sum[i] += p; 
		} 
		p_acc *= p_sum[i]; 
	} 
	prob_of_being_accurate = p_acc; 
	for (i = 0; i < dimension; i++) { 
		for (j = 0; j < samples.size (); j++) { 
			if (i < probe.size ()) distance = points[j][i] - probe[i]; 
			else distance = 0; 
			p = normpdf (distance, 0, stddev[j][i]); 
			if (p_sum[i]) p /= p_sum[i]; 
			else p = 1.0 / samples.size (); 
			p_val[i] += p * samples[j]; 
		} 
		p_res *= p_val[i]; 
	} 
	return p_res; 
} 
double DataMap::P (std::vector<double> probe) { 
	double useless = 0; 
	return P (probe, useless); 
} 
std::vector<double> DataMap::delP (std::vector<double> probe) { 
	std::vector<double> x = probe; 
	std::vector<double> x2; 
	std::vector<double> dP; 
	std::vector<double> del; 
	double p; 
	unsigned int i; 
	x.resize (dimension, 0); 
	dP.resize (dimension, 0); 
	p = P (x); 
	for (i = 0; i < dimension; i++) { 
		x2 = x; 
		x2[i] += discretization; 
		dP[i] = P (x2) - p; 
	} 
	del = dP; 
	for (i = 0; i < dimension; i++) del[i] /= discretization; 
	return del; 
} 
std::vector<double> DataMap::find (double likelihood, double error, std::vector<double> init_x0) { 
	std::vector<double> x0 = init_x0; 
	std::vector<double> x1 = x0; 
	std::vector<double> Pprime; 
	double Pvalue; 
	unsigned int max_tries = 64; 
	unsigned int i, j; 
	for (i = 0; i < max_tries; i++) { 
		Pvalue = P (x0); 
		if (std::abs (Pvalue - likelihood) <= error) break; // Close enough. 
		Pprime = delP (x0); 
		for (j = 0; j < dimension; j++) x1[j] = x0[j] - (Pvalue - likelihood) / Pprime[j]; // Newton's method. 
		x0 = x1; 
	} 
	return x0; 
} 
std::vector<double> DataMap::find (double likelihood, double error) { 
	std::vector<double> x0; 
	x0.resize (dimension, 0); 
	return find (likelihood, error, x0); 
} 

std::vector<double> DataMap::averageX (unsigned int from, unsigned int to) { 
	unsigned int i, j;
	unsigned int stop = to;
	std::vector<double> sum; 
	sum.resize (dimension, 0); 
	if (!stop) stop = samples.size ();
	for (i = from; i < stop; i++) {
		for (j= 0; j < dimension; j++) sum[j] += points[i][j]; 
	}
	for (j= 0; j < dimension; j++) sum[j] /= (stop - from); 
	return sum; 
} 
double DataMap::average (unsigned int from, unsigned int to) { 
	unsigned int i; 
	unsigned int stop = to; 
	double sum = 0; 
	if (!stop) stop = samples.size (); 
	for (i = from; i < stop; i++) { 
		sum += samples[i]; 
	} 
	return sum / (stop - from); 
} 

void DataMap::addSample (std::vector<double> point, std::vector<double> std_dev, double sample) { 
	std::shared_ptr<DataMapExtra> nothing = nullptr; 
	addSample (point, std_dev, sample, nothing); 
} 
void DataMap::addSample (std::vector<double> point, std::vector<double> std_dev, double sample, std::shared_ptr<DataMapExtra> extra) {
	std::vector<double> point_array; 
	unsigned int i; 
	extras.push_back (extra); 
		point_array.resize (dimension, 0); 
		for (i = 0; i < point.size () && i < dimension; i++) point_array[i] = point[i]; 
	points.push_back (point_array); 
		point_array.resize (dimension, 1); 
		for (i = 0; i < std_dev.size () && i < dimension; i++) point_array[i] = std_dev[i]; 
	stddev.push_back (point_array); 
	samples.push_back (sample); 
} 

double DataMap::normpdf (double x, double mean, double stddev) { 
	// Normal distribution: 
	return (1 / (stddev * std::sqrt (2 * PI))) * std::exp (-(x - mean) * (x - mean) / (2 * stddev * stddev));
} 

