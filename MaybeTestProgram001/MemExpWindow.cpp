#include "include\MemExpWindow.hpp" 

#ifdef NDEBUG 
// Non-debug build. 
#define USE_DDRAW 1 
#else 
#define USE_DDRAW 0 
#endif 

#if USE_DDRAW 
#include <ddraw.h> 
#endif 

namespace MemExp {
	std::default_random_engine generator ((unsigned int)(std::chrono::system_clock::now ().time_since_epoch ().count ())); 
} 

MemExp::Window::Window () : log (L"log.txt") { 
	/*HRESULT (*DirectDrawCreateEx) (
		_In_   GUID FAR *lpGUID,
		_Out_  LPVOID *lplpDD,
		_In_   REFIID iid,
		_In_   IUnknown FAR *pUnkOuter
	); 
	HMODULE ddraw = LoadLibrary (L"ddraw.dll"); 
	DirectDrawCreateEx = (HRESULT (*) (GUID FAR *, LPVOID *, REFIID, IUnknown FAR *))GetProcAddress (ddraw, "DirectDrawCreateEx"); */
	DWORD freq; 
#if USE_DDRAW 
	IDirectDraw7 * dd; 
#endif 
	timerThread = std::make_shared<MemExp::TimerThread> (this); 
	timerThread->self = timerThread; 
	stage = 0; 
	// Log ... 
	writeLog (L"Window Object Created; "); 
#if USE_DDRAW 
	/*if (DirectDrawCreateEx)*/ DirectDrawCreate (NULL, (LPDIRECTDRAW *)(&dd_ptr), NULL); 
	/*else*/// dd_ptr = NULL; 
	//FreeLibrary (ddraw); 
#else 
	dd_ptr = nullptr; 
#endif 
	settings.dblParams[L"Diagonal"] = 4; // Default is 4" diagonal. 
	settings.dblParams[L"Frequency"] = 0; 
	settings.dblParams[L"Constraint Standard Deviation"] = 1.0 / 16; 
	settings.dblParams[L"Sample Standard Deviation"] = 1; 
	settings.dblParams[L"Target Probability"] = 0.75; 
	settings.intParams[L"Instructions Font Size"] = 28; 
	settings.intParams[L"Left and Right Padding"] = 32; 
	settings.intParams[L"Top and Bottom Padding"] = 32; 
	settings.intParams[L"Welcome-Text Font Size"] = 32; 
	settings.intParams[L"Prime Font Size"] = 10; 
	settings.intParams[L"Target Font Size"] = 10; 
	settings.intParams[L"Mask Font Size"] = 10; 
	settings.intParams[L"Sign Font Size"] = 10; 
	settings.intParams[L"Answer Option Font Size"] = 10; 
	settings.intParams[L"Correct Checkmark Size"] = 72 * 2; 
	settings.intParams[L"Incorrect \"X\" Size"] = 72 * 2; 
	settings.strParams[L"Prime Font Face"] = L"Arial"; 
	settings.strParams[L"Target Font Face"] = L"Arial"; 
	settings.strParams[L"Mask Font Face"] = L"Arial"; 
	settings.strParams[L"Sign Font Face"] = L"Arial"; 
	settings.strParams[L"Answer Option Font Face"] = L"Arial"; 
	settings.strParams[L"ID"] = L""; 
	settings.strParams[L"Show"] = L""; 
	settings.fromTextFile (L"Settings.txt"); 
	/*if (!settings.dblParams[L"Constraint Standard Deviation"]) 
		settings.dblParams[L"Constraint Standard Deviation"] = 1 >> 4; 
	if (!settings.dblParams[L"Sample Standard Deviation"]) 
		settings.dblParams[L"Sample Standard Deviation"] = 1 >> 3; */
	// The constraints: 
	dataMap.resize (1); 
	dataMap[0].addSample ({ 0 }, { 
		settings.dblParams[L"Constraint Standard Deviation"] 
	}, 0.5); // If user cannot see the target at all, can only guess, so 1/2 correct. 
	dataMap[0].addSample ({ 1 }, { 
		settings.dblParams[L"Constraint Standard Deviation"] 
	}, 1.0); // If can see target for "very long" time, approx. every answer correct. 
#if USE_DDRAW 
	dd = (IDirectDraw7 *)dd_ptr; 
	dd->GetMonitorFrequency (&freq); 
	monitor_freq = freq; 
#else 
	monitor_freq = settings.dblParams[L"Frequency"]; 
	if (!monitor_freq) monitor_freq = 60; 
#endif 
	screenWidth = GetSystemMetrics (SM_CXSCREEN); 
	screenHeight = GetSystemMetrics (SM_CYSCREEN); 
	log << "Monitor Frequency: " << monitor_freq << std::endl; 
	log << "Screen Resolution: " << screenWidth << " by " << screenHeight << std::endl; 
	log << std::endl; 
	log << "Monitor Diagonal (Read From Settings.txt): " << settings.dblParams[L"Diagonal"] << std::endl; 
	settings.dblParams[L"ppi"] = std::sqrt (screenWidth * screenWidth + screenHeight * screenHeight) /
		settings.dblParams[L"Diagonal"]; 
	log << "PPI: " << settings.dblParams[L"ppi"] << std::endl; 
	log << std::endl; 
	log << "Constraint Standard Deviation: " << settings.dblParams[L"Constraint Standard Deviation"] << std::endl; 
	log << "Sample Standard Deviation: " << settings.dblParams[L"Sample Standard Deviation"] << std::endl; 
	log << std::endl; 
	log << std::endl; 
	Command toBegin; 
	Command cInstructions; 
	LIST::IfStream sInstructions (L"instructions.txt", std::ios::binary); 
	std::wstring tInstructions = L""; 
	if (sInstructions.stream.is_open ()) { 
		std::wstring line; 
		do { 
			if (tInstructions.size () > 65536) break; 
			line = sInstructions.getLine (); 
			tInstructions += line; 
		} while (sInstructions.canRead ()); 
	} 
	toBegin.text = L"Press the [space bar] to begin. "; 
	toBegin.vAlign = AlignBottom; 
	toBegin.hAlign = AlignCenter; 
	toBegin.bottom = 1; 
	toBegin.setColor (255, 255, 255); 
	toBegin.fontSize = 32; 
	cInstructions.vAlign = AlignTop; 
	cInstructions.textType = Multiline | WordBreak; 
	cInstructions.text = tInstructions; 
	cInstructions.setColor (255, 255, 255); 
	cInstructions.fontSize = settings.intParams[L"Instructions Font Size"]; 
	SlideTypes::SingleCenteredWord slide; 
	slide.setWord (cInstructions.text + L"\r\n\r\n" + toBegin.text); 
	timerThread->addSlide (std::make_shared<decltype (slide)> (slide)); 
	paddingLeft = settings.intParams[L"Left and Right Padding"]; 
	paddingTop = settings.intParams[L"Top and Bottom Padding"]; 
	paddingRight = paddingLeft; 
	paddingBottom = paddingTop; 
	backgroundR = 0; 
	backgroundG = 0; 
	backgroundB = 0; 
	alreadyStarted = false; 
	nowWelcoming = false; 
	waitVR = true; 
} 


MemExp::Window::~Window () { 
	timerThread->close (); 
} 

void MemExp::Window::addWordList (std::shared_ptr<WordData> list) { 
	lists.push_back (list); 
	writeLog (L"Word List Added; "); 
} 
void MemExp::Window::addGenerator (std::shared_ptr<CommandGenerator> gen) { 
	stages.push_back (gen); 
	stages[stages.size () - 1]->monitorFreq = monitor_freq; 
	stages[stages.size () - 1]->settings = &settings; 
	if (dataMap.size () < stages.size ()) dataMap.resize (stages.size ()); 
} 

void MemExp::Window::writeLog (std::wstring msg) { 
	double atTime = timerThread->getTime (); 
	//log << "Time: " << timerThread->time << "; Message: " << msg << std::endl; 
	log << "Time: " << atTime << "; Message: " << msg << std::endl; 
} 
void MemExp::Window::ddLogFeedback (HRESULT result) {
	log << "Return Value: "; 
	switch (result) { 
#if USE_DDRAW 
		case DDERR_INVALIDOBJECT:
			log << "Invalid Object";
			break;
		case DDERR_INVALIDPARAMS:
			log << "Invalid Parameters";
			break;
		case DDERR_UNSUPPORTED:
			log << "Unsupported";
			break;
		case DDERR_VERTICALBLANKINPROGRESS:
			log << "Vertical Blank in Progress";
			break;
		case DDERR_WASSTILLDRAWING: 
			log << "\"Was Still Drawing\""; 
			break; 
#endif 
		default:
			log << result;
	} 
	log << std::endl; 
}

void MemExp::Window::onmouseup (int clientX, int clientY) { 
	writeLog (L"Mouse Button Released; "); 
	log << "\tCoordinates: " << clientX << ", " << clientY << std::endl; 
} 
void MemExp::Window::onchar (wchar_t keycode) { 
	Command userAnswer; 
	Number now = CommandGenerator::getTime (this); 
	writeLog (L"Key Pressed; "); 
	log << "\tKey: " << keycode << std::endl; 
	if ((keycode | 32) == 'd') { 
		std::ofstream ofs ("distribution.bin", std::ios::out | std::ios::trunc | std::ios::binary); 
		double x; 
		double p; 
		double acc; 
		unsigned int i; 
		for (i = 0; i < 1024; i++) { 
			x = (double)i / 1024; 
			p = dataMap[0].P ({ x }, acc); 
			ofs.write ((char *)(&x), sizeof (double)); 
			ofs.write ((char *)(&p), sizeof (double)); 
			ofs.write ((char *)(&acc), sizeof (double)); 
		} 
		return; 
	} 
	if (now < nextAllowedInput) { 
		// No input allowed yet. 
		// User must wait until the inputs are allowed again. 
		return; 
	} 
	if (stage < stages.size ()) { 
		if ((keycode | 32) == 'z' || keycode == '/' || keycode == 0xC2BF) { 
			if ((keycode | 32) == 'z') { // The (| 32) makes the letter lowercase if it isn't already. 
				writeLog (L"User's Answer: Left"); 
				userAnswer = stages[stage]->ansLeft; 
			} else if (keycode == '/' || keycode == 0xC2BF) { 
				writeLog (L"User's Answer: Right"); 
				userAnswer = stages[stage]->ansRight; 
			} 
			checkAnswer (userAnswer); 
		} else if (keycode == 'J') { 
			//writeLog (L"J start; "); 
			if (showString == L"max") show (L""); 
			else show (L"max"); 
			//writeLog (L"J stop; "); 
		} else if (keycode == ' ') { 
			// The space-bar was pressed. 
			if (nowWelcoming) { 
				Command dummy; 
				checkAnswer (dummy, false); 
				nowWelcoming = false; 
			} 
			if (!alreadyStarted) { 
				Command blank; 
				//generateCycle (CommandGenerator::timerDelay (this)); 
				blank.type = L"init"; 
				blank.text = L""; 
				checkAnswer (blank); 
				alreadyStarted = true; 
			} 
		} 
	} else { 
		destroy (); 
	} 
} 

unsigned int MemExp::Window::countDataMaps () { 
	return dataMap.size (); 
} 
DataMap & MemExp::Window::getDataMap (unsigned int index) { 
	if (index >= dataMap.size ()) dataMap.resize (index + 1); 
	return dataMap[index]; 
} 

void MemExp::Window::drawWin32 (HDC hdc) { 
	std::shared_ptr<SlideSheet> slide = timerThread->getCurrentSlide (); 
	std::wstringstream ss; 
	Command cmd; 
	unsigned int i; 
	HRESULT waitForVBlankR; 
	HRESULT scanLineResult; 
	DWORD scanLine = NULL; 
	RECT r; 
	Box client = getClientBox (); 
	Number primeDuration = 0; 
	Number targetDuration = 0; 
	Number answerDuration = 0; 
	if (stage < stages.size ()) { 
		
		if (primeDuration && !prevPrimeDuration) { 
			stages[stage]->primeDrawn = CommandGenerator::getTime (this); 
		} else if (prevPrimeDuration && !primeDuration) { 
			stages[stage]->primeClear = CommandGenerator::getTime (this); 
		} 
		if (targetDuration && !prevTargetDuration) { 
			stages[stage]->targetDrawn = CommandGenerator::getTime (this); 
		} else if (prevTargetDuration && !targetDuration) { 
			stages[stage]->targetClear = CommandGenerator::getTime (this); 
		} 
		if (answerDuration && !prevAnswerDuration) { 
			stages[stage]->answerDrawn = CommandGenerator::getTime (this); 
		} else if (prevAnswerDuration && !answerDuration){ 
			stages[stage]->answerClear = CommandGenerator::getTime (this); 
		} 
		prevPrimeDuration = primeDuration; 
		prevTargetDuration = targetDuration; 
		prevAnswerDuration = answerDuration; 
	} 
#if USE_DDRAW 
	IDirectDraw7 * dd= (IDirectDraw7 *)dd_ptr; 
	if (waitVR) { 
		writeLog (L"Entered drawing procedure. Attempting to wait for vertical retrace. "); 
		waitForVBlankR = dd->WaitForVerticalBlank (DDWAITVB_BLOCKBEGIN, NULL); 
		if (waitForVBlankR == DD_OK) { 
			writeLog (L"Done waiting for vertical retrace. "); 
		} else {
			writeLog (L"WaitForVerticalBlank Error; "); 
			ddLogFeedback (waitForVBlankR); 
		}
	}
#endif 
	writeLog (L"Drawing Window; "); 
#if USE_DDRAW 
	if (dd) scanLineResult= dd->GetScanLine (&scanLine); 
	if (scanLineResult == DD_OK) { 
		//log << "Current Processor Tick Count: " << (int)GetTickCount () << std::endl; 
		log << "Current Monitor Scan Line: " << (int)scanLine << std::endl;
	} else { 
		log << "\tGetScanLine Error; "; 
		ddLogFeedback (scanLineResult); 
	} 
#endif 
	// Clear: 
		r.left = 0; 
		r.top = 0; 
		r.right = client.w; 
		r.bottom = client.h; 
	HBRUSH hBrush = CreateSolidBrush (RGB (backgroundR, backgroundG, backgroundB)); 
	//FillRect (hdc, &r, (HBRUSH)(COLOR_WINDOW + 1)); 
	FillRect (hdc, &r, hBrush); 
	DeleteObject (hBrush); 
	// Adjust for the padding: 
	client.x += paddingLeft; 
	client.y += paddingTop; 
	client.w -= (paddingLeft + paddingRight); 
	client.h -= (paddingTop + paddingBottom); 
	// Draw: 
	auto nowPlaying = timerThread->getCurrentSlide (); 
	if (nowPlaying) { 
		log << "Drawing [time: " << timerThread->getTime () << 
			"]:  " << nowPlaying->toString () << std::endl << std::endl; 
		nowPlaying->win32draw (hdc, client); 
	} 
	else
		log << "Warning:  Now Playing is NULL; " << std::endl; 
	//for (i = 0; i < list.size (); i++) { 
	//	cmd = list[i]; 
	//	cmd.win32draw (hdc, client); 
	//	ss.str (L""); 
	//	//log << "\tDrawing: " << cmd.toString () << std::endl; 
	//	ss << "\tDrawing: "; 
	//	writeLog (ss.str () + L"... "); 
	//} 
#if USE_DDRAW 
	if (list.size ()) { 
		//log << "\t----" << std::endl; 
		if (dd) scanLineResult = dd->GetScanLine (&scanLine); 
		//log << "Current Processor Tick Count: " << (int)GetTickCount () << std::endl; 
		ss.str (L""); 
		//log << "Current Monitor Scan Line: " << (int)scanLine << std::endl; 
		ss << "Current Monitor Scan Line: " << (int)scanLine; 
		writeLog (ss.str ()); 
	} 
#endif 
	log << std::endl; 
} 

void MemExp::Window::generateCycle (Number delay) { 
	if (!lists.size ()) return; 
	NewTree<Command> copy; 
	Number finishTime = 0; 
	DWORD lastErr; 
	DWORD scanLine; 
	HRESULT scanLineResult; 
#if USE_DDRAW 
	IDirectDraw7 * dd = (IDirectDraw7 *)dd_ptr; 
#endif 
	log << std::endl << std::endl; 
	writeLog (L"Generating new command cycle ... "); 
#if USE_DDRAW 
	if (dd) scanLineResult = dd->GetScanLine (&scanLine); 
	if (scanLineResult == DD_OK) {
		log << "Current Monitor Scan Line: " << (int)scanLine << std::endl;
	} else {
		lastErr = GetLastError ();
		log << "\tGetScanLine Error; ";
		ddLogFeedback (scanLineResult); 
		log << "Last Error: " << lastErr;
		log << std::endl;
	} 
#endif 
	while (stage >= stages.size ()) stage -= stages.size (); 
	stages[stage]->monitorFreq = monitor_freq; 
	stages[stage]->generate (this, delay); 
	/*log << "\tCommands in Tree: " << std::endl; 
	if (!timerThread->commands.size ()) log << "\t\t" << "<none>" << std::endl; 
	else { 
		copy = timerThread->commands; 
		while (copy.size ()) log << "\t\t" << copy.pop (false).toString () << std::endl; 
	} */
	log << std::endl << std::endl; 
	clearActualTimes (); 
} 
void MemExp::Window::clearActualTimes () { 
	stages[stage]->primeDrawn = 0; 
	stages[stage]->primeClear = 0; 
	stages[stage]->targetDrawn = 0; 
	stages[stage]->targetClear = 0; 
	stages[stage]->answerDrawn = 0; 
	stages[stage]->answerClear = 0; 
} 
void MemExp::Window::checkAnswer (Command answer, bool needPutFeedback) { 
	if (needPutFeedback) stages[stage]->putFeedback (answer, getDataMap (stage), 
															this, CommandGenerator::timerDelay (this)); 
	CommandGenerator * prev = nullptr; 
	unsigned int i; 
	if (!alreadyStarted) { 
		if (stage > 0) prev = stages[stage - 1].get (); 
		stages[stage]->prepTime (this, getDataMap (stage), prev); 
	} 
	while (stage < stages.size () && stages[stage]->done ()) { 
		stages[stage]->wrapUp (this, getDataMap (stage)); 
		stage++; 
		if (stage > 0) prev = stages[stage - 1].get (); 
		if (stage < stages.size ()) 
			stages[stage]->prepTime (this, getDataMap (stage), prev); 
	} 
	if (stage < stages.size ()) { 
		if (!stages[stage]->welcomeDone) 
			stages[stage]->postWelcome (this); 
	} else { 
		Command done; 
		DataMap & map = getDataMap (0); 
		std::wstringstream ss; 
		ss << "All done! \r\nYou may now be free. \r\n\r\n"; 
		for (i = 0; i < stages.size (); i++) { 
			double avg = map.average (2); 
			std::vector<double> avgX = map.averageX (2); 
			ss << "Round 1"; 
			if (!i) ss << " (Alignment Stage)"; 
			else ss << "(Block " << i << ")"; 
			ss << " Results: \r\n\r\n"; 
			ss << "Average Score: " << (std::round (1000 * avg) / 10) << "%" << std::endl; 
			ss << "Average Target Duration: " << (std::round (1000 * avgX[0]) / 1000) << std::endl; 
			ss << std::endl; 
			avg = map.average (map.points.size () - 10); 
			avgX = map.averageX (map.points.size () - 10); 
			ss << "Average Score of Last 10 Answers: " << (std::round (1000 * avg) / 10) << "%" << std::endl; 
			ss << "Average of Last 10 Target Durations: " << (std::round (1000 * avgX[0]) / 1000) << std::endl; 
			ss << "\r\n\r\n"; 
		} 
		ss << "\r\n\r\n\r\n\r\n\r\n"; 
		ss << "Press any key to exit. "; 
		SlideTypes::SingleCenteredWord doneSlide; 
		doneSlide.command ().textType = Multiline; 
		doneSlide.setWord (ss.str ()); 
		timerThread->addSlide (std::make_shared<decltype (doneSlide)> (doneSlide)); 
	} 
} 

