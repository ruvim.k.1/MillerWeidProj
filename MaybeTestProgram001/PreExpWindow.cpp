#include "include/PreExpWindow.hpp" 

#include <sstream> 
#include <string> 

#define ID_BTN_BEGIN 1 

PreExp::Window::Window () { 
	
} 
PreExp::Window::~Window () { 

} 

void PreExp::Window::oncreate () { 
	inputs.push_back (CreateWindowEx (WS_EX_CLIENTEDGE, L"EDIT", L"", WS_CHILD | WS_VISIBLE, 
		0, 0, 0, 0, hWnd, NULL, hInst, NULL)); 
	buttons.push_back (CreateWindowExW (WS_EX_CLIENTEDGE, L"BUTTON", L"Begin", 
		WS_CHILD | WS_VISIBLE | BS_USERBUTTON | BS_CENTER | BS_VCENTER, 
		0, 0, 0, 0, hWnd, (HMENU)ID_BTN_BEGIN, hInst, NULL)); 
	SetFocus (inputs[0]); 
	movesize (); 
} 
void PreExp::Window::ondestroy () { 
	unsigned int i; 
	std::wstring text = forceAlphanumeric (); 
	settings.strParams[L"id"] = text; 
	for (i = 0; i < inputs.size (); i++) DestroyWindow (inputs[i]); 
	for (i = 0; i < buttons.size (); i++) DestroyWindow (buttons[i]); 
	inputs.clear (); 
	buttons.clear (); 
} 

void PreExp::Window::onkeyup (wchar_t keycode) { 
	if (keycode == VK_TAB) { 
		SetFocus (buttons[0]); 
	} else { 
		forceAlphanumeric (); 
	} 
} 

void PreExp::Window::onresize (int w, int h) { 
	InvalidateRect (hWnd, NULL, true); 
	movesize (); 
} 
void PreExp::Window::oncommand (int type, int id) { 
	std::wstring text; 
	if (type == BN_CLICKED) { 
		if (id == ID_BTN_BEGIN) { 
			text = forceAlphanumeric (); 
			if (!text.size ()) { 
				MessageBox (hWnd, L"Alphanumeric values only, please. ", L"", MB_OK); 
				return; 
			} 
			destroy (); 
		} 
	} 
} 

void PreExp::Window::drawWin32 (HDC hdc) { 
	RECT r; 
	Box client = getClientBox (); 
	std::wstringstream stream; 
	// Prepare the text to draw: 
	stream.str (L""); 
	stream << "Anonymous ID: "; 
	// Set up the rectangle: 
	r.left = client.x; 
	r.top = client.y; 
	r.right = client.w; 
	r.bottom = client.h; 
	DrawText (hdc, stream.str ().c_str (), stream.str ().size (), &r, 0); 
} 

void PreExp::Window::movesize () { 
	unsigned int i; 
	unsigned int btnArea = 60; 
	unsigned int x, y, w, h; 
	Box client = getClientBox (); 
	for (i = 0; i < inputs.size (); i++) { 
		x = 100; 
		y = 2 + (client.h - btnArea - 4) * i / inputs.size (); 
		w = client.w - 100 - 2; 
		h = (client.h - btnArea - 4) / inputs.size (); 
		SetWindowPos (inputs[i], NULL, x, y, 
					  w, h, SWP_NOZORDER); 
	} 
	for (i = 0; i < buttons.size (); i++) { 
		x = 2 + (client.w - 4) * i / buttons.size (); 
		y = client.h - btnArea + 4; 
		w = (client.w - 4) / buttons.size (); 
		h = btnArea - 8; 
		SetWindowPos (buttons[i], NULL, x, y, w, h, SWP_NOZORDER); 
	} 
} 

std::wstring PreExp::Window::forceAlphanumeric () { 
	std::wstring text; 
	wchar_t string1[512]; 
	unsigned int i; 
	GetWindowText (inputs[0], string1, 511); 
	for (i = 0; i < 512 && string1[i]; i++) { 
		if (string1[i] >= 65 && string1[i] < 65 + 26) text.push_back (string1[i]); 
		if (string1[i] >= 97 && string1[i] < 97 + 26) text.push_back (string1[i]); 
		if (string1[i] >= 48 && string1[i] < 48 + 10) text.push_back (string1[i]); 
	} 
	SetWindowText (inputs[0], text.c_str ()); 
	return text; 
} 

