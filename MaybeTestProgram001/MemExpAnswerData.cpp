#include "include/MemExpWindow.hpp" 

MemExp::AnswerData::AnswerData () { 
	initialize (nullptr); 
} 
MemExp::AnswerData::AnswerData (AnswerData & from) { 
	initialize (&from); 
} 
MemExp::AnswerData::AnswerData (AnswerData * from) { 
	initialize (from); 
} 
MemExp::AnswerData::~AnswerData () { 

} 

void MemExp::AnswerData::initialize (AnswerData * from) { 
	if (from) { 
		clockTime = from->clockTime; 
		primeTime = from->primeTime; 
		targetTime = from->targetTime; 
		answerTime = from->answerTime; 
		stddevUsed = from->stddevUsed; 
		pCorrect = from->pCorrect; 
		condition = from->condition; 
		prime = from->prime; 
		target = from->target; 
		left = from->left; 
		right = from->right; 
		return; 
	} 
	clockTime = primeTime = targetTime = answerTime = stddevUsed = pCorrect = 0; 
	condition = (unsigned int)((int)(-1)); 
} 

std::wstring const MemExp::AnswerData::getCSV () { 
	std::wstringstream stream; 
	const char * comma = ","; 
	long long clockData = 0; 
	*((double *)(&clockData)) = clockTime; 
	// Note:  We need a comma after every value except for the last one. 
	stream << "0x" << std::hex << clockData << comma; 
	stream << primeTime << comma; 
	stream << targetTime << comma; 
	stream << answerTime << comma; 
	stream << stddevUsed << comma; 
	stream << pCorrect << comma; 
	stream << prime.text << comma; 
	stream << target.text << comma; 
	stream << left.text << comma; 
	stream << right.text << comma; 
	stream << condition; 
	return stream.str (); 
} 
bool MemExp::AnswerData::setCSV (std::wstring row) { 
	long long clockData = 0; 
	unsigned int i = 0; 
	std::vector<std::wstring> list; 
	std::wstringstream stream; 
	std::wstring text; 
	wchar_t comma = ','; 
	do { 
		text = L""; 
		for (; i < row.size () && row.at (i) != comma; i++) text.push_back (row.at (i)); 
		list.push_back (text); 
		if (i < row.size () && row.at (i) == comma) i++; 
	} while (i < row.size ()); 
	if (list.size () >= 10) { 
		// Need at least 10 items. 
		// Parse the numerical variables: 
		stream.str (list[0]); 
		stream >> std::hex >> clockData; 
		clockTime = *((double *)(&clockData)); 
		stream.str (L""); // Empty the stream. 
		stream.clear (); // Clear error flags. 
		stream.str (list[1]); 
		/*std::wstring test = stream.str (); 
		unsigned int position = stream.tellg (); */
		stream >> primeTime; 
		stream.str (L""); // Empty the stream. 
		stream.clear (); // Clear error flags. 
		stream.str (list[2]); 
		/*std::wstring test2 = stream.str (); 
		unsigned int position2 = stream.tellg (); */
		stream >> targetTime; 
		stream.str (L""); // Empty the stream. 
		stream.clear (); // Clear error flags. 
		stream.str (list[3]); 
		stream >> answerTime; 
		stream.str (L""); // Empty the stream. 
		stream.clear (); // Clear error flags. 
		stream.str (list[4]); 
		stream >> stddevUsed; 
		stream.str (L""); // Empty the stream. 
		stream.clear (); // Clear error flags. 
		stream.str (list[5]); 
		stream >> pCorrect; 
		// Parse the text variables: 
		prime.text = list[6]; 
		target.text = list[7]; 
		left.text = list[8]; 
		right.text = list[9]; 
		// If available, parse more things: 
		if (list.size () >= 11) { 
			// Parse the condition index: 
			condition = 0; 
			stream.str (L""); 
			stream.clear (); 
			stream.str (list[10]); 
			stream >> condition; 
		} else { 
			condition = 0; 
		} 
		// Success: 
		return true; 
	} else return false; 
} 

