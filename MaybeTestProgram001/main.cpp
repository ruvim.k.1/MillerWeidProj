#include "include/MemExpWindow.hpp" 
#include "include/PreExpWindow.hpp" 

#include <direct.h> 

#define GLOBAL_CONDITIONS_FILENAME L"Data/global_condition_tracking.txt" 

void saveConditions (std::wstring filename, std::vector<unsigned int> conditions) { 
	unsigned int i; 
	std::wofstream oConditions (filename.c_str (), std::ios::trunc); 
	for (i = 0; i < conditions.size (); i++) { 
		if (i) oConditions << "\r\n"; // One condition per line. 
		oConditions << conditions[i] << " "; // A space after the number, just to make the text "prettier" (style preference). 
	} 
} 
std::vector<std::vector<unsigned int> > loadConditionsG (std::wstring filename, std::vector<std::wstring> & names) { 
	std::vector<std::vector<unsigned int> > result; 
	std::vector<unsigned int> conditions; 
	std::wstringstream ss; 
	std::wstring line; 
	std::wstring name; 
	unsigned int number; 
	LIST::IfStream input (filename.c_str ()); 
	// Clear the names output array: 
	names.clear (); 
	// Pre-allocate some memory: 
	names.reserve (1024); 
	result.reserve (1024); 
	conditions.reserve (16); 
	// Main loop to parse each line: 
	while (input.canRead ()) { 
		line = input.getLine (); 
		// Clear the variables: 
		conditions.clear (); 
		name.clear (); 
		// Clear string stream: 
		ss.str (L""); 
		ss.clear (); 
		// Set the line text as the string to stream from: 
		ss.str (line); 
		// Do some processing: 
		ss >> name; 
		do { 
			number = 0; 
			ss >> number; 
			if (number) conditions.push_back (number); 
		} while (number); 
		// Check if blank: 
		if (!name.size ()) continue; 
		if (!conditions.size ()) continue; 
		// Add the data to where it should go: 
		result.push_back (conditions); 
		names.push_back (name); 
	} 
	return result; 
} 
void saveConditionsG (std::wstring filename, std::wstring id, std::vector<unsigned int> conditions) { 
	std::vector<std::wstring> names; 
	std::vector<std::vector<unsigned int> > allConditions = loadConditionsG (filename, names); 
	std::wofstream ofs (filename.c_str ()); 
	unsigned int i, j; 
	bool haveDone = false; 
	for (i = 0; i < names.size (); i++) { 
		ofs << names[i] << " "; 
		if (names[i] == id) { 
			for (j = 0; j < conditions.size (); j++) ofs << conditions[j] << " "; 
			ofs << std::endl; 
			haveDone = true; 
			continue; 
		} 
		for (j = 0; j < allConditions[i].size (); j++) ofs << allConditions[i][j] << " "; 
		ofs << std::endl; 
	} 
	if (!haveDone) { 
		ofs << id << " "; 
		for (j = 0; j < conditions.size (); j++) ofs << conditions[j] << " "; 
		ofs << std::endl; 
	} 
} 
void setupConditionRandomizer (CountRandomizer & r) { 
	// Just resize to account for 4 conditions: 
	r.resize (4); 
} 
void setupConditionRandomizerState (CountRandomizer & r, std::wstring id, std::wstring global_conditions_filename) { 
	std::vector<std::wstring> names; 
	std::vector<std::vector<unsigned int> > condition_data = loadConditionsG (global_conditions_filename, names); 
	unsigned int i; 
	for (i = 0; i < names.size (); i++) { 
		if (names[i] == id) continue; 
		r.dec_stack (condition_data[i][0] - 1); 
	} 
} 
void setupConditions (std::vector<std::shared_ptr<MemExp::CommandGenerator> > & generators, std::vector<unsigned int> conditions) { 
	unsigned int i; 
	unsigned int c = 1; 
	MemExp::CommandGeneratorStage2 * gen = NULL; 
	for (i = 0; i < generators.size (); i++) { 
		gen = (MemExp::CommandGeneratorStage2 *)(generators[i].get ()); 
		if (i < conditions.size ()) c = conditions[i]; // 'c' is the condition number. 
		gen->condition.resize (0); // Resize to 0 so we can push_back (); more efficient would be [], but we want more convenient. 
		if (c == 1) { 
			// Long: 
			gen->condition.push_back ({ 8, 24, 8 }); 
			// Short: 
			gen->condition.push_back ({ 8, 24, 8 }); 
		} else if (c == 2) { 
			// Long: 
			gen->condition.push_back ({ 8, 8, 24 }); 
			// Short: 
			gen->condition.push_back ({ 8, 8, 24 }); 
		} else if (c == 3) { 
			// Long: 
			gen->condition.push_back ({ 8, 8, 24 }); 
			// Short: 
			gen->condition.push_back ({ 8, 24, 8 }); 
		} else if (c == 4) { 
			// Long: 
			gen->condition.push_back ({ 8, 24, 8 }); 
			// Short: 
			gen->condition.push_back ({ 8, 8, 24 }); 
		} 
	} 
} 

int WINAPI WinMain (HINSTANCE a, HINSTANCE b, LPSTR c, int d) { 
	unsigned int i; 
	int redo_pre = 0; 
	MemExp::Window win; // Window for the experiment. 
	PreExp::Window pre; // Questionair before the experiment. 
	std::shared_ptr<MemExp::WordData> list = std::make_shared<MemExp::WordData> (L"6_letters4.txt"); // Load words. 
	CountRandomizer conditionRandomizer; 
	// Do the Quest debug business: 
	MemExp::QuestImplementation quest; 
	std::ofstream debugQuest ("quest_debug_1.txt"); 
	for (i = 0; i < 1024; i++) { 
		double x = ((double)i / 16) - 18; 
		debugQuest << "Input: " << x << "; S: " << quest.S (x) << "; F: " << quest.F (x) << "; \r\n";
	} 
	//return 0; 
	// Do the pre-experimental business: 
	pre.setText (L"Pre-Experimental Questions"); 
	pre.setSize (400, 200); 
	if (!win.settings.strParams[L"ID"].size ()) { 
		do { 
			pre.messageLoop (); 
			// Check the ID: 
			if (!pre.settings.strParams[L"id"].size ()) { 
				int ans = MessageBox (NULL, L"Invalid identifier entered. \r\n\r\nClick OK to try again or Cancel to quit. ", 
									  NULL, MB_OKCANCEL | MB_ICONERROR); 
				if (ans == IDOK) redo_pre = 1; 
				else return -1; 
			} else redo_pre = 0; 
		} while (redo_pre); 
		win.settings.strParams[L"ID"] = pre.settings.strParams[L"id"]; 
	} else pre.settings.strParams[L"id"] = win.settings.strParams[L"ID"]; 
	// Ensure the directories exist: 
	std::wstringstream path; 
	path.str (L""); 
	path << "Data"; 
	_wmkdir (path.str ().c_str ()); 
	path << "/" << pre.settings.strParams[L"id"]; 
	_wmkdir (path.str ().c_str ()); 
	// Initialize the command generators: 
	std::shared_ptr<MemExp::CommandGenerator> basic = std::make_shared<MemExp::CommandGenerator> (); 
	std::vector < std::shared_ptr<MemExp::CommandGenerator> > block; 
	basic->welcomeFiles.push_back (L"Text/stage1-welcome.txt"); 
	// Add 6 blocks: 
	for (i = 0; i < 6; i++) { 
		std::wstringstream stream; 
		block.push_back (std::make_shared<MemExp::CommandGeneratorStage2> ()); 
		stream << path.str (); 
		stream << "/block"; 
		stream << (1 + i); 
		stream << ".csv"; 
		block[i]->csvFile = stream.str (); 
		stream.str (L""); 
		stream.clear (); 
		stream << "Text"; 
		stream << "/welcome-" << (1 + i) << ".txt"; 
		block[i]->welcomeFiles.push_back (stream.str ()); 
	} 
	// Because we'll need this in a moment, set up the condition randomizer: 
	setupConditionRandomizer (conditionRandomizer); 
	setupConditionRandomizerState (conditionRandomizer, win.settings.strParams[L"ID"], GLOBAL_CONDITIONS_FILENAME); 
	// Read the conditions from the file, or generate new conditions: 
	std::vector<unsigned int> conditions;
	std::vector<unsigned int> brandNewConditions;
	std::wstring pConditions = path.str () + L"/conditions.txt";
	LIST::IfStream sConditions (pConditions.c_str ());
	std::default_random_engine gen ((unsigned int)(std::chrono::system_clock::now ().time_since_epoch ().count ()));
	std::uniform_real_distribution<double> u0_4 (0, 4);
	if (sConditions.stream.is_open ()) {
		// Read from the file: 
		std::wstringstream stream;
		std::wstring line;
		unsigned int cond;
		while (sConditions.canRead ()) {
			// One condition per line, so ... 
			line = sConditions.getLine ();
			cond = 0;
			stream.str (L""); // Clear text. 
			stream.clear (); // Clear errors. 
			stream.str (line); // Set text = the line just read. 
			stream >> cond;
			if (!cond) continue; // cond == 0 --> one of two things: 1) stream did not read at all (cond=0 above), 
			// or 2) the condition was 0; well, 0 is, by our own definition of the condition, not allowed, so ... 
			conditions.push_back (cond);
		}
		sConditions.stream.close ();
	}
	unsigned int prevConditionsSize = conditions.size (); // Save number of conditions read from file. 
	// Now, if not enough conditions read from file, go ahead and add more to ensure a condition per block: 
	unsigned int condition;
	for (i = 0; i < block.size (); i++) { 
		if (!i) condition = 1 + conditionRandomizer.random (); 
		if (!conditions.size ()) { 
			conditions.push_back (condition); 
		} 
		// For some reason, the experiment is supposed to have all four conditions equal; 
		// so, that is why we just set everything we can to be the same. 
		brandNewConditions.push_back (condition); 
		if (i < conditions.size ()) continue; 
		conditions.push_back (conditions[0]); 
		// We have two sets of conditions: 
		// 1. One that may have had some of its conditions read from the file. 
		// 2. A brand new one, generated here and now. 
		// We have both for now because we do not know whether the user will click 'yes' or 'no' 
		// when prompted about whether or not to recover the data. 
	}
	// Check if we had to add any or if only had to read from file: 
	if (prevConditionsSize < conditions.size ()) {
		// If had to add something that was not originally part of the file, re-save things to the file: 
		saveConditions (pConditions.c_str (), conditions);
	}
	// Add the word list and the generators: 
	win.addWordList (list);
	win.addGenerator (basic); // Add the basic generator that will last for the 60 alignment trials. 
	for (i = 0; i < block.size (); i++)
		win.addGenerator (block[i]); // Add the block generator that will last for the 80 trials. 
	basic->csvFile = path.str () + L"/basic.csv";
	unsigned int totalDataEntryCount = 0;
	unsigned int basicNumber = basic->countEntriesInCSV (basic->csvFile);
	std::vector<unsigned int> block_counts;
	for (i = 0; i < block.size (); i++) {
		block_counts.push_back (block[i]->countEntriesInCSV (block[i]->csvFile));
		totalDataEntryCount += block_counts[i];
	}
	totalDataEntryCount += basicNumber;
	if (totalDataEntryCount) {
		std::wstringstream ss;
		unsigned int renameError;
		ss << "It appears there is experimental data available for recovery. \r\n\r\n";
		if (basicNumber) {
			ss << "Stage 1 (" << basic->csvFile << ") has " << basicNumber << " data ";
			if (basicNumber == 1) ss << "entry";
			else ss << "entries";
			ss << ". \r\n";
		}
		for (i = 0; i < block.size (); i++) {
			if (!block_counts[i]) continue;
			ss << "Block " << (1 + i) << " (" << block[i]->csvFile << ") has ";
			ss << block_counts[i] << " data ";
			if (block_counts[i] == 1) ss << "entry";
			else ss << "entries";
			ss << ". \r\n";
		}
		if (prevConditionsSize) {
			ss << "\r\n";
			ss << "In addition, there is some additional minor data available for recovery. ";
			ss << "\r\n";
		}
		ss << "\r\n";
		ss << "Do you want to recover the data? ";
		int resp = MessageBox (NULL, ss.str ().c_str (), L"Message from Memory Experiment App", MB_YESNO);
		if (resp == IDYES) {
			// User clicked "yes"; need to recover data. 
			setupConditions (block, conditions);
			if (basicNumber) basic->loadFromCSV (basic->csvFile, win.getDataMap (0));
			for (i = 0; i < block.size (); i++) {
				if (!block_counts[i]) continue;
				block[i]->loadFromCSV (block[i]->csvFile, win.getDataMap (1 + i));
			}
		} else {
			// User clicked "no" or exited message box (somehow); 
			// rename old file to something else ... : 
			auto now = std::chrono::system_clock::now ().time_since_epoch ().count ();
			renameError = 0;
			if (basicNumber) {
				ss.str (L"");
				ss << basic->csvFile.substr (0, basic->csvFile.find (L".csv"));
				ss << "-" << now << ".csv";
				renameError |= _wrename (basic->csvFile.c_str (), ss.str ().c_str ());
				std::ofstream clear (basic->csvFile.c_str (), std::ios::out | std::ios::trunc); // Clear all data. 
			}
			for (i = 0; i < block.size (); i++) {
				if (!block_counts[i]) continue;
				ss.str (L"");
				ss.clear ();
				ss << block[i]->csvFile.substr (0, block[i]->csvFile.find (L".csv"));
				ss << "-" << now << ".csv";
				renameError |= _wrename (block[i]->csvFile.c_str (), ss.str ().c_str ());
				std::ofstream clear (block[i]->csvFile.c_str (), std::ios::out | std::ios::trunc);
			}
			// Rename old conditions: 
			if (prevConditionsSize) {
				conditions.resize (prevConditionsSize);
				ss.str (L"");
				ss.clear ();
				ss << pConditions.substr (0, pConditions.find (L".txt"));
				ss << "-" << now << ".txt";
				renameError |= _wrename (pConditions.c_str (), ss.str ().c_str ());
			}
			// Use new conditions: 
			conditions = brandNewConditions;
			saveConditions (pConditions.c_str (), conditions);
			setupConditions (block, conditions);
			// If there was some sort of rename error anywhere, complain to the user: 
			if (renameError) {
				// Could not rename file. 
				MessageBox (NULL, L"Could not rename one or more data files. ", NULL, MB_OK | MB_ICONERROR);
			}
		}
	} else {
		// If there is no other data to be recovered, then let's not bother 
		// asking the user about the conditions. Let's just use the brand-new ones: 
		conditions = brandNewConditions; // Copy data over from the brand-new one. 
		saveConditions (pConditions.c_str (), conditions); // Save new data. 
		setupConditions (block, conditions);
	}
	// Save the current ID conditions in the global conditions file: 
	saveConditionsG (GLOBAL_CONDITIONS_FILENAME, win.settings.strParams[L"ID"], conditions); 
	// Proceed to the main experiment window: 
	win.setSize (640, 480); 
	win.setText (L"Memory Experiment"); 
	return win.messageLoop (); 
} 
