#include "include/MemExpWindow.hpp" 

MemExp::CommandGeneratorStage2::CommandGeneratorStage2 () { 
	selected = 0; 
} 

void MemExp::CommandGeneratorStage2::onaddanswer (AnswerData * answer, DataMap & map) { 
	// When an answer from a previous session is loaded, we need to decrement that 
	// particular answer's condition probability, in order to make sure that the 
	// 'condition' object ends up in the same exact state as it was back when the 
	// program was closed last time. The dec_stack () method has the same effect 
	// as though the random () method was called and the return value was that 
	// condition. 
	if (answer->condition < condition.size ()) condition.dec_stack (answer->condition); 
} 
void MemExp::CommandGeneratorStage2::onaddnewanswer (AnswerData * answer, DataMap & map) { 
	answer->condition = selected; 
} 

void MemExp::CommandGeneratorStage2::prepTime (Window * expWindow, DataMap & map, CommandGenerator * prev) { 
	if (!prev) throw 9; // Some sort of error. 
	// Get calculated target duration: 
	targetTimeNow = prev->targetTimeFinal; 
	// Get all words previously asked: 
	alreadyAsked = prev->alreadyAsked; 
} 
void MemExp::CommandGeneratorStage2::wrapUp (Window * expWindow, DataMap & map) { 

} 

bool MemExp::CommandGeneratorStage2::done () { 
	// If no more conditions left, then we're done: 
	if (!condition.count ()) { 
		return true;
	}  else return false;
} 

void MemExp::CommandGeneratorStage2::beginGenerateProcedure () { 
	unsigned int iPrime = 0; 
	unsigned int iMatch = 0; 
	// Pick a random condition: 
	selected = condition.random (); 
	draft->condition = selected; 
	// Decode the condition into two parts: 
	iPrime = selected / 3; // Which prime time to use. 
	iMatch = selected % 3; // Which word should the prime match. 
	// Now execute the condition: 
	exactPrimeTime = avlPrimeTimes[iPrime]; // Set the prime time. 
	// Now set which ones should match: 
	primeNeedMatchFoil = primeNeedMatchTarget = false; // Clear our flags. 
	switch (iMatch) { 
		case 1: 
			// Match target. 
			primeNeedMatchTarget = true; // Set the "match target" flag. 
			break; 
		case 2: 
			// Match foil. 
			primeNeedMatchFoil = true; // Set the "match foil" flag. 
	} 
} 

void MemExp::CommandGeneratorStage2::adjustTargetTime (DataMap & map) { 
	// Do nothing. The target time stays constant for the block trials. 
} 
