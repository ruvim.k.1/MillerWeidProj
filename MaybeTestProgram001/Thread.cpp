#include "include/Window.hpp"


BasicWindow::Thread::Thread () { 
	self = nullptr; 
	needClose = false; 
	hThread= CreateThread (0, 0, &Thread::static_threadEntry, this, 0, &threadID); 
} 


BasicWindow::Thread::~Thread () { 
	
} 

DWORD WINAPI BasicWindow::Thread::static_threadEntry (LPVOID param) {
	Thread * thread = reinterpret_cast<Thread *> (param); 
	std::shared_ptr<Thread> pointer = thread->self; 
	int result= thread->threadEntry (); 
	thread->self = nullptr; 
	return result; 
} 
int BasicWindow::Thread::threadEntry () { 
	// This is pretty much the "do nothing" (except sleep and eat) thread. 
	// Override this threadEntry () method with all the useful functionality to be implemented. 
	while (!needClose) Sleep (100); 
	// If self-destruct variable (selfDestruct) is set, then Thread object will self-destroy upon return. 
	return 0; 
} 

void BasicWindow::Thread::close () { 
	needClose = true; 
} 
