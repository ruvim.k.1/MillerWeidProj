#include "include/MemExpWindow.hpp" 

MemExp::WordData::WordData () { 

} 
MemExp::WordData::WordData (std::wstring filename) : DataFile (filename) { 

} 

std::wstring MemExp::WordData::sampleWord () { 
	std::uniform_real_distribution<double> uniform (0, 1); 
	unsigned int count = items.size (); 
	unsigned int target = (unsigned int)(std::floor ((double)count * uniform (generator))); 
	if (target >= count) return L"Error: Not Found"; 
	return items[target]->value; 
} 
