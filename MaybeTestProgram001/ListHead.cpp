#include "include/ListTextFile.hpp" 

LIST::Head::Head () { 
	ofsFirstItem = 0; 
	itemCount = 0; 
} 

void LIST::Head::update (std::vector<std::shared_ptr<Item> > & list_vector) {
	itemCount = list_vector.size (); 
} 
bool LIST::Head::read (IfStream * file) { 
	wchar_t c; 
	std::wstringstream ss; 
	headerText = file->getLine (); 
	// Load header text into a string stream: 
	ss.str (headerText); 
	ss >> itemCount; // Parse the first integer from the text, and save into the 'itemCount' variable. 
	ofsFirstItem = (unsigned long long)(file->stream.tellg ()); 
	// Set the rest of the string to be the header text template: 
	while (ss) { 
		c = ss.get (); 
		headerTextTemplate.push_back (c); 
	} 
	// Finish the method: 
	return true; 
} 
unsigned long long LIST::Head::write () { 
	std::wstringstream ss; 
	ss.str (L""); 
	ss << itemCount; 
	return ss.str ().size () + headerTextTemplate.size (); 
} 
unsigned long long LIST::Head::write (OfStream * file) { 
	unsigned long long was_at = file->stream.tellp (); 
	unsigned long long written; 
	file->stream << itemCount; 
	file->stream << headerTextTemplate; 
	written = (unsigned long long)(file->stream.tellp ()) - was_at; 
	return written; 
} 
